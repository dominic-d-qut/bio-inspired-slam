% 
NE = 100;
dt = 0.001;
tol = 1e-6;
iter = 5000;

% goal
spikeLocation = 25;

angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);

spikeAngle = rad2deg(angles(spikeLocation));

IN = zeros(NE,200);
IN(spikeLocation,1:100) = 1;
% IN(spikeLocation+40,1:100) = 1;
% IN(spikeLocation+80,1:100) = 0.6;
stdevRange = 1:100;
errors = zeros(1,length(stdevRange));
Es = zeros(length(stdevRange),NE);
goals = zeros(length(stdevRange),NE);
xs = zeros(length(stdevRange), 7);

tic
for k = 1:length(stdevRange)
	stdev = stdevRange(k)
	goal = normpdf(rad2deg(angles), spikeAngle, stdev) + normpdf(rad2deg(angles), spikeAngle+360, stdev) + normpdf(rad2deg(angles), spikeAngle-360, stdev);
	goal = goal ./ max(goal);
	goals(k,:) = goal;

	% x0 = [0.02,-1.5,0.005,-7.5,stdev,6,-8,0.88,-4];
	x0 = [-1.5,-7.5,stdev,6,-8,0.88,-4];
% 	% x = fminsearch(@(x)RunStableAttractor(x, IN, spikeAngle, stdev, NE, dt, 0), x0, optimset('display', 'iter'));
	x = fminsearch(@(x)RunStableAttractor(x, IN, goal, NE, dt, 0), x0, optimset('display', 'none','MaxIter',iter,'MaxFunEvals',iter,'tolfun',tol,'tolx',tol));
	% x = fminsearch(@(x)RunStableAttractor(x, IN, spikeAngle, stdev, NE, dt, 0), x0);
	xs(k,:) = x.';
	% finalError = RunStableAttractor(x, IN, spikeAngle, stdev, NE, dt, 1);
	[finalError,E] = RunStableAttractor(x, IN, goal, NE, dt, 0);
	errors(k) = finalError;
	Es(k,:) = E;
% 	sum(E)
end
toc
plot(stdevRange, errors);
title(sprintf('error vs stddev (stable network) \n network with 100 neurons, peak at 86.4 degrees'))
ylabel('SSE between steady state and target normal pdf')
xlabel('target stddev (\circ)')

figure('position', [50,50,1200,600]);
ax1 = axes('position', [0.05,0.05,0.25,0.9]);
ax2 = axes('position', [0.35,0.05,0.6,0.9]);
for k = 1:length(stdevRange)
	cla(ax1);
	hold(ax1,'on');
	plot(ax1,stdevRange, errors, 'b');
	plot(ax1,stdevRange(k), errors(k), 'rx');
	hold(ax1,'off');
	xlim(ax1,[0,stdevRange(end)]);
	title(ax1, 'Best stable state SSE vs stdev');
	
	
	cla(ax2);
	hold(ax2,'on');
	stem(ax2,rad2deg(angles),Es(k,:));
	plot(ax2,rad2deg(angles),goals(k,:));
	hold(ax2,'off');
	legend(ax2,'response','goal');
	xlim(ax2,[0,360]);
	ylim(ax2,[0,1]);
	xticks(ax2,[0:45:360]);
	title(ax2, sprintf('actual response vs. goal for std = %.0d degrees',stdevRange(k)));
	
	drawnow;
	
	f = gcf;
	frame = getframe(f);
	im = frame2im(frame);
	[imind,cm] = rgb2ind(im,256);
	if k == 1 
		 imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.5); 
	else 
		 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.5); 
	end
end

