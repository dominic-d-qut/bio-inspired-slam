function y = circularnormpdf(x, mu, sigma, max)
	diff = x - mu;
	z = reshape(min([abs(diff(:)), abs(diff(:) + max), abs(diff(:) - max)], [], 2), size(diff));
	twoVar = 2*sigma^2;
	y = 1/sqrt(pi*twoVar) * exp(-z.^2 / twoVar);