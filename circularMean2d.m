function [mean] = circularMean2d(distribution)
% 	if max(distribution) > 2*min(distribution)
% 		distribution = distribution - min(distribution);
% 	end

% 	if all(all(distribution == 0))
% 		mean = [0,0];
% 		return;
% 	end

	anglesX = linspace(0, 2*pi, size(distribution,2)+1);
	anglesX = anglesX(1:end-1);
	anglesY = linspace(0, 2*pi, size(distribution,1)+1);
	anglesY = anglesY(1:end-1);
	[x, y] = meshgrid(anglesX,anglesY);
	
	if sum(sum(distribution)) ~= 0
		normDist = distribution ./ sum(sum(distribution));
	else
		normDist = distribution;
	end
		
	meanX = mod(1 / 2 / pi * angle(sum(sum(exp(1i*x) .* normDist))), 1);
	meanY = mod(1 / 2 / pi * angle(sum(sum(exp(1i*y) .* normDist))), 1);
	if abs(meanX - 1) < 1e-6
		meanX = 0;
	end
	if abs(meanY - 1) < 1e-6
		meanY = 0;
	end
	mean = [meanX, meanY];
end