function plot2dMatrixValues(mat, ax1, ax2)
	imagesc(ax1, ax2, mat);
	set(gca,'Ydir','Normal');
	textStrings = strtrim(cellstr(num2str(mat(:), '%0.2f')));
	[x, y] = meshgrid(ax1,ax2);
	hStrings = text(x(:), y(:), textStrings(:), 'HorizontalAlignment', 'center');
	xlabel('cell i')
	ylabel('cell j')
end