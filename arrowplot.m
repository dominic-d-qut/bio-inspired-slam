function arrowplot(x, y, theta, linesize, arrowsize, varargin)

if size(x,2) > 1
	x = x.';
end
if size(y,2) > 1
	y = y.';
end
if size(theta,2) > 1
	theta = theta.';
end

endPoint = [x,y];
startPoint = endPoint - linesize * [cos(theta), sin(theta)];
arrow1 = endPoint - arrowsize * [cos(theta+pi/4), sin(theta+pi/4)];
arrow2 = endPoint - arrowsize * [cos(theta-pi/4), sin(theta-pi/4)];

plot([endPoint(:,1),startPoint(:,1),endPoint(:,1),arrow1(:,1),endPoint(:,1),arrow2(:,1)].', ...
	[endPoint(:,2),startPoint(:,2),endPoint(:,2),arrow1(:,2),endPoint(:,2),arrow2(:,2)].', varargin{:});