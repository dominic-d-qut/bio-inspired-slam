function robotSimulation(params, NE, tLength, showsim)

dt = 0.001; % s
IN = zeros(NE,tLength);
IN(1,1:100) = 1; % set initial position to 0 deg
angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);

% robot state
theta = 0; % deg
FOV = 30; % deg
vRobot = 100; % deg / s
tStartRot = 0.2;
tEndRot = tLength*dt - 0.2;

% landmarks
LTheta = [90, 215]; % deg

% network params
tauE = 0.02; % time constant, excitatory neurons
tauI = 0.005; % time constant, inhibitory neuron
gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
weightL = 20;
stdevL = 3;

E = zeros(NE,1); % allocate space for NE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,1); % I -> E weights
W_IE = weightIE * ones(1,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix(NE, stddevConst, Wconst); % E -> E weights
W_V = BuildVelocityWeightMatrix(NE, 0.5, 1);

lastMean = 0;

for t = 1:tLength
	% update the robot theta
	if t > tStartRot/dt && t <= tEndRot/dt
		vRot = vRobot;
	else
		vRot = 0;
	end
	theta = wrapTo360(theta + dt*vRot);
	
	L = zeros(NE,1);
	% can the robot see the landmark
	for l = 1:length(LTheta)
		lTheta = LTheta(l);
		bearing = wrapTo180(lTheta - theta);
		if abs(bearing) <= FOV
			robotTheta = lTheta - bearing;
			L = L + weightL * normpdf(rad2deg(angles.'), robotTheta, stdevL) - weightL / 360;
	% 		topAng = find(rad2deg(angles) >= robotTheta,1);
	% 		if isempty(topAng)
	% 			topAng = 1;
	% 		end
	% 		botAng = mod(topAng - 2, NE) + 1;
	% 		
	% 		L(topAng) = weightL * wrapTo180(robotTheta - rad2deg(angles(botAng))) / rad2deg(angles(2)); % angles(1) is 0
	% 		L(botAng) = weightL * wrapTo180(rad2deg(angles(topAng)) - robotTheta) / rad2deg(angles(2)); % angles(1) is 0
		end
	end
	
	% update the network
	lastE = E;
	v = vRot / 50;%64;%.5333333; % rough conversion factor for 20deg network
	VE = W_EI * I + W_EE * E + gammaE + IN(:,t) + (-v * W_V) * E + L;
	VI = W_II * I + W_IE * E + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
	
	diff = sum(abs(lastE - E)) / sum(E);
	[mean, stdev] = circularStats(E);
	deltaX = wrapTo180(mean - lastMean);
	deltaV = deltaX / dt;
% 	deltaVs(t) = deltaV;
% 	if t > tStart/dt
% 		sumDeltaX = sumDeltaX + deltaX;
% 	end
	lastMean = mean;


	if showsim == 1 && mod(t, 5) == 0
		polarplot(angles, E, 'b');
		rlim([0,1]);
% 		ylim([0 1])
% 		xlim([0,360]);
		hold on
% 		stem(rad2deg(angles), IN(:,t), 'r');
		polarplot(angles, L, 'k');
		polarplot([deg2rad(theta),deg2rad(theta)], [0,1], 'm');
		polarplot([deg2rad(mean),deg2rad(mean)], [0,1], 'r');
		polarplot(deg2rad(LTheta), ones(1, length(LTheta)), 'kx');
% 		legend('network', 'landmark input', 'actual direction', 'predicted direction', 'landmarks')
		hold off
		title(sprintf('state: mean = %.1f, std = %.1f, v = %.1f',mean,stdev,deltaV));
		drawnow;
% 		f = gcf;
% 		frame = getframe(f);
% 		im = frame2im(frame);
% 		[imind,cm] = rgb2ind(im,256);
% 		if t == 5 
% 			 imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.05); 
% 		else 
% 			 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.05); 
% 		end
	end
end
