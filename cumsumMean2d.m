function [xy] = cumsumMean2d(A)
	xMean = mean(A, 1);
	yMean = mean(A, 2).';
	
	xMean = xMean ./ sum(xMean);
	yMean = yMean ./ sum(yMean);
	
	cumsumX = [0,cumsum(xMean)];
	cumsumY = [0,cumsum(yMean)];
	
	topX = find(cumsumX > 0.5, 1);
	topY = find(cumsumY > 0.5, 1);
	
	x = (topX - 2 + (0.5 - cumsumX(topX-1))/(cumsumX(topX) - cumsumX(topX-1))) / size(A, 2);
	y = (topY - 2 + (0.5 - cumsumY(topY-1))/(cumsumY(topY) - cumsumY(topY-1))) / size(A, 1);
	
	xy = [x, y];
end