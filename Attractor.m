% function RESULTS = Attractor(figure_to_show)
%
% Attractor
%
% One dimensional attractor sample code
% Written for Matlab 5.0
%
% Written by A. David Redish, 1998,
% for the book _Beyond the Cognitive Map_,
% published by MIT Press, 1999.
figure_to_show = 1;

% global dt
% global W_EI W_IE W_II W_EE
% global tauE gammaE tauI gammaI
%------------------------------
% set random seed
%------------------------------
rand('state', 0);


%------------------------------
% PARAMETERS
%------------------------------
NE = 75; % Number of excitatory neurons [FIXED]
dt = 0.001; % time step, in ms [FIXED]

tauE = 0.01; % time constant, excitatory neurons
gammaE = -1.5; % tonic inhibition, excitatory neurons
tauI = 0.002; % time constant, inhibitory neuron
gammaI = -7.5; % tonic inhibition, inhibitory neuron
stddevConst = 15; % intra-ring weighting field width (degrees)
Wconst = 6.0; % intra-ring weighting field strength
weightEI = -8.0; % How much does the inhibitory neuron inhibit all others
weightIE = 0.880; % How much do all the excitory neurons inhibit the inhibitory one
weightII = -4.0; % How much does the inhibitory neuron excite itself

%------------------------------
% VARIABLES
%------------------------------
E = zeros(NE,1); % allocate space for NE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,1); % I -> E weights
W_IE = weightIE * ones(1,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix(NE, stddevConst, Wconst); % E -> E weights


%------------------------------
% Input functions
%------------------------------
switch figure_to_show
case 1 % figure A.1
	IN = cat(1, ones(100,1) * rand(1,NE), zeros(100,NE))';
case 2 % figure A.2
	IN0 = exp(-(((1:NE) - 35).^2)/25);
	IN1 = ones(100,1) * IN0;
	IN0 = 2 * exp(-(((1:NE) - 40).^2)/25);
	IN2 = ones(200,1) * IN0;
	IN = cat(1,IN1,IN2)';
case 3 % figure A.3
	IN0 = exp(-(((1:NE) - 20).^2)/25);
	IN1 = ones(100,1) * IN0;
	IN0 = exp(-(((1:NE) - 60).^2)/25);
	IN2 = ones(200,1) * IN0;
	IN = cat(1,IN1,IN2)';
case 4 % figure A.4
	IN0 = exp(-(((1:NE) - 20).^2)/25);
	IN1 = ones(100,1) * IN0;
	IN0 = 2 * exp(-(((1:NE) - 60).^2)/25);
	IN2 = ones(200,1) * IN0;
	IN = cat(1,IN1,IN2)';
case 6 % figure A.6
	IN0 = 0.5 * (0.1 * rand(1,NE) + exp(-(((1:75) - 25).^2)/40));
	IN1 = 0.5 * (0.1 * rand(1,NE) + exp(-(((1:75) - 45).^2)/40));
	IN = (ones(150,1) * (IN0 + IN1))';
case 7 % figure A.7
	IN0 = 0.5 * (0.1 * rand(1,NE) + exp(-(((1:75) - 29).^2)/40));
	IN1 = 0.5 * (0.1 * rand(1,NE) + exp(-(((1:75) - 41).^2)/40));
	IN = (ones(150,1) * (IN0 + IN1))';
otherwise
	disp('Unknown input set')
end

%----------------------------
% CYCLE
%----------------------------
steps = size(IN,2);
RESULTS = zeros(NE,steps);
figure
for t=1:steps
	lastE = E;
	VE = W_EI * I + W_EE * E + gammaE + IN(:,t);
	VI = W_II * I + W_IE * E + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
	RESULTS(:,t) = E;
	
	diff = sum(abs(lastE - E)) / sum(E);
	
	% draw in real time
	stem(E);
	ylim([0 1])
	hold on
	stem(IN(:,t));
	stem(I);
	hold off
	drawnow;
end

%-----------------------------
% DRAW
%------------------------------
figure(1)
clf
surfl(RESULTS);
shading interp
view([-30 75])
colormap(bone)
xlabel('time')
ylabel('neurons')
zlabel('activity')
set(gca,'Xtick',[])
set(gca,'Ytick',[])
set(gca,'Ztick',[])
figure(2)
clf
timeslices = 1:(steps/10):steps;
for i=1:8
subplot(8,1,i);
fill([1 1:NE NE],[0 RESULTS(:,timeslices(i))' 0],'k');
axis([1 NE 0 1]);
xlabel([num2str(timeslices(i)-1) ' steps'])
set(gca,'Xtick',[]);
set(gca,'Ytick',[]);
end


%------------------------------
% Build Weight Matrix
%------------------------------
function W = BuildWeightMatrix(NE, stddevConst, Wconst)
	variance = stddevConst^2 / (360^2) * NE^2;
	i = ones(NE,1) * (1:NE);
	j = (1:NE)' * ones(1,NE);
	d_choices = cat(3,abs(j + NE - i), abs(i + NE - j), abs(j - i));
	d = min(d_choices, [], 3);
	W = exp(-d .* d / variance);
	W = Wconst * W./(ones(NE,1) * sum(W));
end

function y = tanhActivation(x)
	y = 0.5 + 0.5*tanh(x);
end