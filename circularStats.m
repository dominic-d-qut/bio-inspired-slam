function [mean, stdev] = circularStats(distribution)
% 	if max(distribution) > 2*min(distribution)
% 		distribution = distribution - min(distribution);
% 	end
	angles = linspace(0, 2*pi, numel(distribution)+1);
	angles = angles(1:end-1);
	
	if(size(distribution,2) == 1)
		angles = angles.';
	end
	
	normDist = distribution ./ sum(distribution);
	
	vectors = exp(i*angles) .* normDist;
	sumVec = sum(vectors);
	
	mean = mod(rad2deg(angle(sumVec)), 360);
	stdev = rad2deg(sqrt(-2*log(abs(sumVec))));
end