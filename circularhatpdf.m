function y = circularhatpdf(x, mu, sigma, max)
	diff = x - mu;
	z = reshape(min([abs(diff(:)), abs(diff(:) + max), abs(diff(:) - max)], [], 2), size(diff));
	y = 1/(sqrt(3*sigma)*pi^(1/4)) .* (1 - (z/sigma).^2 ) .* exp(-z.^2 / (2*sigma^2));