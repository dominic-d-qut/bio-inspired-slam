function W = BuildWeightMatrix2d(NE, stddevConst, Wconst)
	W = zeros(NE,NE,NE,NE);
	variance = (stddevConst).^2;
	[j,i] = meshgrid(1:NE);
	for ii = 1:NE
		for jj = 1:NE
			dx = ii - i;
			dy = jj - j;
			mindx = reshape(min([abs(dx(:)), abs(NE + dx(:)), abs(NE - dx(:))], [], 2), size(dx)) / NE;
			mindy = reshape(min([abs(dy(:)), abs(NE + dy(:)), abs(NE - dy(:))], [], 2), size(dy)) / NE;
			d2 = mindx.^2 + mindy.^2;
			W(:,:,ii,jj) = d2;
		end
	end
 	W = Wconst / (2*pi*variance) * exp(-W / 2 / variance); % divided by 2 to correspond to normal distribution
end