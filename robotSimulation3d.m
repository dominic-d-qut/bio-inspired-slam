function [errorXY, errorTheta, xs, ys, thetas, landmarkEnergies, lastLapErrorXY] = robotSimulation3d(params_HD, params_GC, NE_HD, NE_GC, tLength, showsim)
rng('default'); % seed the random number generator so random noise is the same each time
dt = 0.001; % s

% 1D network angles
angles = linspace(0,2*pi,NE_HD+1);
angles = angles(1:end-1);
anglesSin = sin(angles);
anglesCos = cos(angles);
anglesDeg = rad2deg(angles);

% 2D network axes (same)
ax = linspace(0,1,NE_GC+1);
ax = ax(1:end-1);

% robot state
theta = 0; % deg
x = ax(2);
y = ax(floor((NE_GC + 2)/2));

tStartRot = 0.5; % how long the networks get to settle before the inputs are applied
tEndRot = tLength*dt - 0.1;

IN_HD = zeros(NE_HD, tLength);
IN_HD(1,1:tStartRot/dt-1) = 1;

IN_GC = zeros(NE_GC, NE_GC, tLength);
IN_GC(floor((NE_GC + 2)/2), 2, 1:tStartRot/dt-1) = 1;

vRotRobot = zeros(1,tLength);

Rspeed = 500;
Vspeed = 1;

vRobot = zeros(1,tLength);
vRobot(round(tStartRot/dt):end) = Vspeed;

%% Different simulation environments (uncomment the environment to use)

% office lap length = 4603
% landmarks = [.1 .5 .8 .9 .55 .2 0; .6 .6 .4 -.1 .2 -.1 .3];
% vRotRobot(round(tStartRot/dt)+1:round((tStartRot+0.8/Vspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+0.8/Vspeed)/dt)+1:round((tStartRot+0.8/Vspeed+90/Rspeed)/dt)) = -Rspeed;
% vRotRobot(round((tStartRot+0.8/Vspeed+90/Rspeed)/dt)+1:round((tStartRot+1.3/Vspeed+90/Rspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+1.3/Vspeed+90/Rspeed)/dt)+1:round((tStartRot+1.3/Vspeed+180/Rspeed)/dt)) = -Rspeed;
% vRotRobot(round((tStartRot+1.3/Vspeed+180/Rspeed)/dt)+1:round((tStartRot+1.5/Vspeed+180/Rspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+1.5/Vspeed+180/Rspeed)/dt)+1:round((tStartRot+1.5/Vspeed+240/Rspeed)/dt)) = -Rspeed;
% vRotRobot(round((tStartRot+1.5/Vspeed+240/Rspeed)/dt)+1:round((tStartRot+2.0/Vspeed+240/Rspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+2.0/Vspeed+240/Rspeed)/dt)+1:round((tStartRot+2.0/Vspeed+390/Rspeed)/dt)) = Rspeed;
% vRotRobot(round((tStartRot+2.0/Vspeed+390/Rspeed)/dt)+1:round((tStartRot+2.433/Vspeed+390/Rspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+2.433/Vspeed+390/Rspeed)/dt)+1:round((tStartRot+2.433/Vspeed+480/Rspeed)/dt)) = -Rspeed;
% vRotRobot(round((tStartRot+2.433/Vspeed+480/Rspeed)/dt)+1:round((tStartRot+2.783/Vspeed+480/Rspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+2.783/Vspeed+480/Rspeed)/dt)+1:round((tStartRot+2.783/Vspeed+570/Rspeed)/dt)) = -Rspeed;
% vRotRobot(round((tStartRot+2.783/Vspeed+570/Rspeed)/dt)+1:round((tStartRot+3.283/Vspeed+570/Rspeed)/dt)) = 0;
% vRotRobot(round((tStartRot+3.283/Vspeed+570/Rspeed)/dt)+1:round((tStartRot+3.283/Vspeed+660/Rspeed)/dt)) = -Rspeed;
% vRobot(round(tStartRot/dt)+1:round((tStartRot+0.8/Vspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+0.8/Vspeed)/dt)+1:round((tStartRot+0.8/Vspeed+90/Rspeed)/dt)) = 0;
% vRobot(round((tStartRot+0.8/Vspeed+90/Rspeed)/dt)+1:round((tStartRot+1.3/Vspeed+90/Rspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+1.3/Vspeed+90/Rspeed)/dt)+1:round((tStartRot+1.3/Vspeed+180/Rspeed)/dt)) = 0;
% vRobot(round((tStartRot+1.3/Vspeed+180/Rspeed)/dt)+1:round((tStartRot+1.5/Vspeed+180/Rspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+1.5/Vspeed+180/Rspeed)/dt)+1:round((tStartRot+1.5/Vspeed+240/Rspeed)/dt)) = 0;
% vRobot(round((tStartRot+1.5/Vspeed+240/Rspeed)/dt)+1:round((tStartRot+2.0/Vspeed+240/Rspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+2.0/Vspeed+240/Rspeed)/dt)+1:round((tStartRot+2.0/Vspeed+390/Rspeed)/dt)) = 0;
% vRobot(round((tStartRot+2.0/Vspeed+390/Rspeed)/dt)+1:round((tStartRot+2.433/Vspeed+390/Rspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+2.433/Vspeed+390/Rspeed)/dt)+1:round((tStartRot+2.433/Vspeed+480/Rspeed)/dt)) = 0;
% vRobot(round((tStartRot+2.433/Vspeed+480/Rspeed)/dt)+1:round((tStartRot+2.783/Vspeed+480/Rspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+2.783/Vspeed+480/Rspeed)/dt)+1:round((tStartRot+2.783/Vspeed+570/Rspeed)/dt)) = 0;
% vRobot(round((tStartRot+2.783/Vspeed+570/Rspeed)/dt)+1:round((tStartRot+3.283/Vspeed+570/Rspeed)/dt)) = Vspeed;
% vRobot(round((tStartRot+3.283/Vspeed+570/Rspeed)/dt)+1:round((tStartRot+3.283/Vspeed+660/Rspeed)/dt)) = 0;
% lap = vRotRobot(round(tStartRot/dt)+1:round((tStartRot+3.283/Vspeed+660/Rspeed)/dt));
% lapStraight = vRobot(round(tStartRot/dt)+1:round((tStartRot+3.283/Vspeed+660/Rspeed)/dt));
% lapLength = length(lap);
% lap1Length = lapLength + round(tStartRot/dt);
% n = 1;
% while lap1Length + n*lapLength <= tLength
% 	vRotRobot(lap1Length + (n-1)*lapLength + 1 : lap1Length + n*lapLength) = lap;
% 	vRobot(lap1Length + (n-1)*lapLength + 1 : lap1Length + n*lapLength) = lapStraight;
% 	n = n + 1;
% end

% figure 8 lap length = 3138
landmarks = [1 .8 .3 .5 .4 0.1; .5 1 .9 .45 0 .6];
straightLength = 0.2 / Vspeed;
turnLength = 90 / Rspeed;
vRotRobot(round(tStartRot/dt)+1:round((tStartRot+2*straightLength+(4/pi)*turnLength)/dt)) = 0;
vRotRobot(round((tStartRot+2*straightLength+(4/pi)*turnLength)/dt)+1:round((tStartRot+2*straightLength+(1+4/pi)*turnLength)/dt)) = Rspeed;
vRotRobot(round((tStartRot+2*straightLength+(1+4/pi)*turnLength)/dt)+1:round((tStartRot+3*straightLength+(1+4/pi)*turnLength)/dt)) = 0;
vRotRobot(round((tStartRot+3*straightLength+(1+4/pi)*turnLength)/dt)+1:round((tStartRot+3*straightLength+(2+4/pi)*turnLength)/dt)) = Rspeed;
vRotRobot(round((tStartRot+3*straightLength+(2+4/pi)*turnLength)/dt)+1:round((tStartRot+4*straightLength+(2+4/pi)*turnLength)/dt)) = 0;
vRotRobot(round((tStartRot+4*straightLength+(2+4/pi)*turnLength)/dt)+1:round((tStartRot+4*straightLength+(3+4/pi)*turnLength)/dt)) = Rspeed;
vRotRobot(round((tStartRot+4*straightLength+(3+4/pi)*turnLength)/dt)+1:round((tStartRot+6*straightLength+(3+8/pi)*turnLength)/dt)) = 0;
vRotRobot(round((tStartRot+6*straightLength+(3+8/pi)*turnLength)/dt)+1:round((tStartRot+6*straightLength+(4+8/pi)*turnLength)/dt)) = -Rspeed;
vRotRobot(round((tStartRot+6*straightLength+(4+8/pi)*turnLength)/dt)+1:round((tStartRot+7*straightLength+(4+8/pi)*turnLength)/dt)) = 0;
vRotRobot(round((tStartRot+7*straightLength+(4+8/pi)*turnLength)/dt)+1:round((tStartRot+7*straightLength+(5+8/pi)*turnLength)/dt)) = -Rspeed;
vRotRobot(round((tStartRot+7*straightLength+(5+8/pi)*turnLength)/dt)+1:round((tStartRot+8*straightLength+(5+8/pi)*turnLength)/dt)) = 0;
vRotRobot(round((tStartRot+8*straightLength+(5+8/pi)*turnLength)/dt)+1:round((tStartRot+8*straightLength+(6+8/pi)*turnLength)/dt)) = -Rspeed;
lap = vRotRobot(round(tStartRot/dt)+1:round((tStartRot+8*straightLength+(6+8/pi)*turnLength)/dt));
lap1Length = round((tStartRot+8*straightLength+(6+8/pi)*turnLength)/dt);
lapLength = lap1Length - round(tStartRot/dt);
n = 1;
while lap1Length + n*lapLength <= tLength
	vRotRobot(lap1Length + (n-1)*lapLength + 1 : lap1Length + n*lapLength) = lap;
	n = n + 1;
end



lapXs = zeros(size(lap));
lapYs = zeros(size(lap));
lapThetas = zeros(size(lap));
lapX = x;
lapY = y;
lapTheta = theta;
for i = 1:length(lap)
	lapTheta = wrapTo360(lapTheta + dt*lap(i));
	lapX = lapX + dt * Vspeed * cosd(lapTheta);
	lapY = lapY + dt * Vspeed * sind(lapTheta);
	
	lapXs(i) = lapX;
	lapYs(i) = lapY;
	lapThetas(i) = lapTheta;
end



%% network params
gammaE_HD = params_HD(1); % tonic inhibition, excitatory neurons
gammaI_HD = params_HD(2); % tonic inhibition, inhibitory neuron
stddevConst_HD = params_HD(3); % intra-ring weighting field width (degrees)
Wconst_HD = params_HD(4); % intra-ring weighting field strength
weightEI_HD = params_HD(5); % How much does the inhibitory neuron inhibit all others
weightIE_HD = params_HD(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII_HD = params_HD(7); % How much does the inhibitory neuron excite itself
tauE_HD = params_HD(8); % time constant, excitatory neurons
tauI_HD = params_HD(9); % time constant, inhibitory neuron
weightV_HD = params_HD(10);
weightL_HD = params_HD(11);
stdevL_HD = params_HD(12);

gammaE_GC = params_GC(1); % tonic inhibition, excitatory neurons
gammaI_GC = params_GC(2); % tonic inhibition, inhibitory neuron
stddevConst_GC = params_GC(3); % intra-ring weighting field width (degrees)
Wconst_GC = params_GC(4); % intra-ring weighting field strength
weightEI_GC = params_GC(5); % How much does the inhibitory neuron inhibit all others
weightIE_GC = params_GC(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII_GC = params_GC(7); % How much does the inhibitory neuron excite itself
tauE_GC = params_HD(8); % time constant, excitatory neurons
tauI_GC = params_HD(9); % time constant, inhibitory neuron
weightV_GC = params_GC(10);
weightL_GC = params_GC(11);
stdevL_GC = params_GC(12);

E_HD = zeros(NE_HD,1); % allocate space for NE excitatory neurons
I_HD = 0; % one inhibitory neuron
W_EI_HD = weightEI_HD * ones(NE_HD,1); % I -> E weights
W_IE_HD = weightIE_HD * ones(1,NE_HD); % E -> I weights
W_II_HD = weightII_HD; % I -> I weights
W_EE_HD = BuildWeightMatrix(NE_HD, stddevConst_HD, Wconst_HD); % E -> E weights
W_V_HD = BuildVelocityWeightMatrix(NE_HD, 1, weightV_HD);

E_GC = zeros(NE_GC,NE_GC); % allocate space for NExNE excitatory neurons
I_GC = 0; % one inhibitory neuron
W_EI_GC = weightEI_GC * ones(NE_GC,NE_GC); % I -> E weights
W_IE_GC = weightIE_GC * ones(NE_GC,NE_GC); % E -> I weights
W_II_GC = weightII_GC; % I -> I weights
W_EE_GC = BuildWeightMatrix2d(NE_GC, stddevConst_GC, Wconst_GC); % E -> E weights
W_Vx_GC = BuildVelocityWeightMatrix2d(NE_GC, 1, 0, weightV_GC);
W_Vy_GC = BuildVelocityWeightMatrix2d(NE_GC, 0, 1, weightV_GC);

if showsim % if showing the output, can set up figure window here
	figure;
% 	figure('position', [50,50,800,500]);
	% v = VideoWriter('simulation.mp4','MPEG-4');
	% v.FrameRate = 15;
	% open(v);
	subplot(2,2,3);
	plot(landmarks(1,:), landmarks(2,:), 'ko','markersize', 10, 'markerfacecolor', 'black');
	% viscircles(landmarks.', [3,3], 'color', 'k', 'linestyle', '--');
end

% estimated landmark positions are stored as 2D grid cell arrays of probabilities
estLandmarks = zeros(NE_GC, NE_GC, size(landmarks,2));
estLandmarksWrapping = zeros(2, size(landmarks, 2)); % keeps track of whether the landmark position is 0.X or 1.X 
timesLandmarkSeen = zeros(1,size(landmarks,2));

LRange = 0.6; % grids
LFOV = 20; % degrees

% error tracking outputs
% error = 0;
errorXY = 0;
errorTheta = 0;
lastMeanXY = [x, y];
lastMeanAng = theta;
lastLapErrorXY = 0;

wrapX = 0;
wrapY = 0;

% track all the x, y, theta data
xs = zeros(3, tLength);
ys = zeros(3, tLength);
thetas = zeros(3, tLength);
landmarkEnergies = zeros(1, tLength); % how much activity is injected for landmark relocalisation at each time step

firstframe = 1;

% keep track of the noisy x, y, and theta positions (dead reckoning)
xNoise = x;
yNoise = y;
thetaNoise = theta;

% keep track of the velocity of the robot at each time step
vThetas = zeros(size(vRotRobot));
vLinears = zeros(size(vRobot));



% main update loop
for t = 1:tLength
	% update the robot
	
	% no noise
% 	vTheta = vRotRobot(t);
% 	vLinear = vRobot(t);

	% zero-mean noise
% 	vTheta = vRotRobot(t) + 50*randn(1);
% 	vLinear = vRobot(t) + 0.1*randn(1);
	
	% biased noise
	vTheta = vRotRobot(t) + 50*randn(1) + 1;
	vLinear = vRobot(t) + 0.1*randn(1) + 0.05;
	
	vThetas(t) = vTheta;
	vLinears(t) = vLinear;
	
	theta = wrapTo360(theta + dt*vRotRobot(t));
	x = x + dt * vRobot(t) * cosd(theta);
	y = y + dt * vRobot(t) * sind(theta);
	
	thetaNoise = wrapTo360(thetaNoise + dt*vTheta);
	xNoise = xNoise + dt*vLinear * cosd(thetaNoise);
	yNoise = yNoise + dt*vLinear * sind(thetaNoise);
	
	% injected activity due to landmarks is initially 0
	L_HD = zeros(NE_HD,1);
	L_GC = zeros(NE_GC,NE_GC);
	
	% get landmark measurements
	diffL = landmarks - [x;y];
	range = sqrt(sum(diffL.^2,1));
	bearing = wrapTo180(atan2d(diffL(2,:), diffL(1,:)) - theta);
	visibleLandmarks = (range <= LRange) & (abs(bearing) <= LFOV);
	Nvisible = sum(visibleLandmarks);
	estimates = landmarks(:,visibleLandmarks) - [x;y] + lastMeanXY.';
	angle = 0;
	measurements = [];
	cleanMeasurements = [];
	% process any detected landmarks (but only after the time that the network initially has to settle)
	if Nvisible > 0 && t > tStartRot/dt
		for l = 1:size(landmarks,2)
			if visibleLandmarks(l)
				actualRange = norm(landmarks(:,l) - [x;y]);
				actualBearing = wrapTo180(atan2d(landmarks(2,l)-y, landmarks(1,l)-x) - theta);
				cleanMeasurements = [cleanMeasurements; actualRange, actualBearing];
				
				% NOISE
				actualRange = actualRange + (0.1*actualRange) * randn(1);
				actualBearing = actualBearing + (0.1 * actualBearing) * randn(1);
				
				measurements = [measurements; actualRange, actualBearing];
				
				% based on your position and heading estimate, and the range and bearing measurement - where's the landmark?
				estLandmarkPos = lastMeanXY.' + [wrapX; wrapY] + actualRange * [cosd(lastMeanAng+actualBearing);sind(lastMeanAng+actualBearing)];
				
				% make the landmark estimate based on the grid, shifted by the range/bearing (option 1)
% 				estLandmarkGrid = circshift2d(E_GC, NE_GC*actualRange*cosd(lastMeanAng+actualBearing), NE_GC*actualRange*sind(lastMeanAng+actualBearing));
				
				% make the landmark estimate based on the grid and angle distribution, shifted by the range/bearing (option 2) - better?
				bearingDist = circshift1d(E_HD, NE_HD * actualBearing / 360);
				estLandmarkGrid = circshift2dAngleDistr(E_GC, bearingDist, NE_GC*actualRange);

				timesLandmarkSeen(l) = timesLandmarkSeen(l) + 1;
				estLandmarks(:,:,l) = (timesLandmarkSeen(l)-1)/timesLandmarkSeen(l) .* estLandmarks(:,:,l) + 1/timesLandmarkSeen(l) .* estLandmarkGrid;

				estLandmarksWrapping(:,l) = round(estLandmarkPos - circularMean2d(estLandmarks(:,:,l)).'); 
				estLandmarkUpdated = estLandmarksWrapping(:,l).' + circularMean2d(estLandmarks(:,:,l));

% 				if timesLandmarkSeen(l) > 100 (if you only want to use landmarks you've seen xxx times before
					estAngle = lastMeanAng + actualBearing;
					estBearing = wrapTo180(atan2d(estLandmarkUpdated(2)-lastMeanXY(2)-wrapY, estLandmarkUpdated(1)-lastMeanXY(1)-wrapX) - lastMeanAng);

					pos = estLandmarkUpdated.' - actualRange * [cosd(estAngle);sind(estAngle)];				
					angle = lastMeanAng - (actualBearing - estBearing);

					% Head direction
					distr = circularnormpdf(anglesDeg, wrapTo360(angle), stdevL_HD, 360).';
					if sum(distr) ~= 0
						distr = distr - sum(distr)/length(distr);
					else
						% fallback for if the stdev of the landmark injection is tiny - can lead to 0 total activity
						% this is nonlinear behaviour that should probably be removed
						[~,I] = min(abs(anglesDeg - wrapTo360(angle)));
						distr(I) = 1;				
					end
					L_HD = L_HD + weightL_HD * distr;

					% Grid cells
					distr2 = circularnormpdf2(ax, ax, [mod(pos(1),1),mod(pos(2),1)], [stdevL_GC, stdevL_GC], [1,1]);
					if sum(distr2) ~= 0
						distr2 = distr2 - sum(sum(distr2))/numel(distr2);
					else
						[~,Ix] = min(abs(ax - mod(pos(1),1)));
						[~,Iy] = min(abs(ax - mod(pos(2),1)));
						distr2(Iy,Ix) = 1;				
					end
					L_GC = L_GC + weightL_GC * distr2;
% 				end
			end
		end
	end
	
	
	% update the network
	v_HD = -vTheta;
	emat_HD = W_V_HD .* v_HD + W_EE_HD;
	VE_HD = W_EI_HD * I_HD + emat_HD * E_HD + IN_HD(:,t) + gammaE_HD + L_HD;
	VI_HD = W_II_HD * I_HD + W_IE_HD * E_HD + gammaI_HD;
	FE_HD = tanhActivation(VE_HD); % activation function
	FI_HD = tanhActivation(VI_HD);
	E_HD = E_HD + dt/tauE_HD * (-E_HD + FE_HD);
	I_HD = I_HD + dt/tauI_HD * (-I_HD + FI_HD);
	
	meanAng = fastCircularMean(anglesCos, anglesSin, E_HD);
	
	% can reshape the 4D connection matrix (NExNExNExNE) to a 2D connection matrix (NE^2 x NE^2) for cheaper computation
	% as in StableGridCellAttractor
	exciteMat = W_EE_GC + -vLinear*(cosd(meanAng) * W_Vx_GC + sind(meanAng) * W_Vy_GC);
	excite = exciteMat .* E_GC;
	exciteV = reshape(sum(sum(excite)), size(E_GC));
	exciteV = reshape(W_E_1D * reshape(E, NE^2, 1), size(E));
	VE_GC = W_EI_GC * I_GC + exciteV + IN_GC(:,:,t) + gammaE_GC + L_GC;
	VI_GC = W_II_GC * I_GC + sum(sum(W_IE_GC .* E_GC)) + gammaI_GC;
	FE_GC = tanhActivation(VE_GC); % activation function
	FI_GC = tanhActivation(VI_GC);
	E_GC = E_GC + dt/tauE_GC * (-E_GC + FE_GC);
	I_GC = I_GC + dt/tauI_GC * (-I_GC + FI_GC);
	
	meanXY = circularMean2d(E_GC);
	
	% this assumes that the network doesn't travel more than half the grid per timestep.
	if lastMeanXY(1) < 0.25 && meanXY(1) > 0.75
		wrapX = wrapX - 1;
	elseif lastMeanXY(1) > 0.75 && meanXY(1) < 0.25
		wrapX = wrapX + 1;
	end
	
	if lastMeanXY(2) < 0.25 && meanXY(2) > 0.75
		wrapY = wrapY - 1;
	elseif lastMeanXY(2) > 0.75 && meanXY(2) < 0.25
		wrapY = wrapY + 1;
	end
	
	lastMeanXY = meanXY;
	lastMeanAng = meanAng;
	
	% store all the data
	xs(:,t) = [x; wrapX + meanXY(1); xNoise];
	ys(:,t) = [y; wrapY + meanXY(2); yNoise];
	thetas(:,t) = [theta; meanAng; thetaNoise];
	landmarkEnergies(t) = sum(sum(abs(L_GC)));
	
	errorX = mod(x - meanXY(1) + 0.5, 1) - 0.5;
	errorY = mod(y - meanXY(2) + 0.5, 1) - 0.5;
	errorXY = errorXY + sqrt(errorX.^2 + errorY.^2);
	errorTheta = errorTheta + abs(wrapTo180(meanAng-theta))/180;
	if (tLength - t <= lapLength)
		lastLapErrorXY = lastLapErrorXY + sqrt(errorX.^2 + errorY.^2);
	end

	% if displaying the simulation
	% mod(t, 20) shows only every 20th frame
	if showsim == 1 && mod(t, 20) == 0 && t*dt >= tStartRot
		set(gcf, 'color', 'w');
		
		subplot(2,2,1)
		stem(rad2deg(angles), E_HD);
		xlim([0,360]);
		xticks([0:45:360]);
		ylim([0,1]);
		title('Head Direction Network Activity');
		
		subplot(2,2,2)
		imagesc(ax,ax,E_GC, [0,1]);
		title('Grid Cell Network Activity');
		set(gca,'Ydir','Normal');
		
		subplot(2,2,3)
		plot(lapXs, lapYs, 'g');
		hold on
		plot(landmarks(1,:), landmarks(2,:), 'ko','markersize', 10, 'markerfacecolor', 'black');
		landmarksToPlot = [];
		for l = 1:size(landmarks,2)
			if timesLandmarkSeen(l) ~= 0
				xy = circularMean2d(estLandmarks(:,:,l)) + estLandmarksWrapping(:,l).';
				landmarksToPlot = [landmarksToPlot; xy];
			end
		end
		if numel(landmarksToPlot) > 0
			plot(landmarksToPlot(:,1), landmarksToPlot(:,2), 'mx', 'markersize', 15);
		end
		
		arrow_size = 0.1;
		arrowplot(x, y, deg2rad(theta), 0.15, 0.07, 'm', 'linewidth', 1.5);
		arrowplot(meanXY(1) + wrapX, meanXY(2) + wrapY, deg2rad(meanAng), 0.15, 0.07, 'b', 'linewidth', 1.5);
		plot(xs(2,1:40:t), ys(2,1:40:t), 'b.');
		hold off
		title('Top Down View')
		axis equal
 		
		p = subplot(2,2,4);
		cla(p);
		colormap(p, 'gray');
		patch('Faces', [1,2,3,4], 'Vertices', [-1,-1,1,1;0,-1,-1,0].', 'FaceVertexCData', [.4;.7;.7;.4], 'FaceColor', 'interp', 'EdgeColor', 'none');
		rectangle('position', [-1,0,2,1], 'facecolor', 0.8*ones(1,3), 'edgecolor', 0.8*ones(1,3));
		for n = 1:size(cleanMeasurements, 1)
			xL = -cleanMeasurements(n, 2) / LFOV;
			sizeL = 0.1 / cleanMeasurements(n, 1);
			
			rectX = linspace(-1,1,20).';
			verts = [xL+rectX*sizeL/2, ones(size(rectX)); xL+rectX*sizeL/2, -sizeL*ones(size(rectX)) - sizeL/4*cos(rectX*pi/2)];
			q = (1:length(rectX)-1).';
			faces = [q, q+1, q+length(rectX)+1, q+length(rectX)];
			
			patch('Faces', faces, 'Vertices', verts, 'FaceVertexCData', [0.4+.6*abs(rectX.^2);0.6+.4*abs(rectX.^2)], 'FaceColor', 'interp', 'EdgeColor', 'none');
		end
		xlim([-1,1])
		ylim([-1,1])
		caxis([0,1])
		title('First Person View')
		axis off;
		
		drawnow;
		
		% to render a video of the sim (need close it below)
% 		if firstframe == 1
% 			vid = VideoWriter('full simulation.mp4','MPEG-4');
% 			vid.FrameRate = 15;
% 			open(vid);
% 			firstframe = 0;
% 		end
	end
end
% close(vid);

errorX = mod(x - meanXY(1) + 0.5, 1) - 0.5;
errorY = mod(y - meanXY(2) + 0.5, 1) - 0.5;
thetaError = mod(theta - meanAng + 180, 360) - 180;
errors = [errorX, errorY, thetaError];