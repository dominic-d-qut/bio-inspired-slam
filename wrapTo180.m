function wrappedAngle = wrapTo180(angle)
% 	angle(abs(angle) > 180) = mod(angle(abs(angle) > 180)+180, 360) - 180;
% 	wrappedAngle = angle;
	wrappedAngle = mod(angle+180, 360) - 180;	
% 	if abs(angle) > 180
% 		wrappedAngle = mod(angle+180, 360) - 180;
% 	else
% 		wrappedAngle = angle;
% 	end
end