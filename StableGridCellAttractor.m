function [error, E, t85, diffSum, vel, acc] = StableGridCellAttractor(params, goal, NE, dt, tLength, spikeLocation, ...
	desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, showsim)

input = zeros(NE,NE,tLength);

ax = linspace(0,1,NE+1);
ax = ax(1:end-1);

tStart = dt;
tEnd = dt * round(0.1 / dt);
input(spikeLocation(1),spikeLocation(2),tStart/dt:tEnd/dt) = 1;

% network params
gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
tauE = params(8); % time constant, excitatory neurons
tauI = params(9); % time constant, inhibitory neuron

E = zeros(NE,NE); % allocate space for NExNE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,NE); % I -> E weights
W_IE = weightIE * ones(NE,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix2d(NE, stddevConst, Wconst); % E -> E weights

W_E_1D = reshape(W_EE, NE^2, NE^2);

lastE = E;
diffSum = 0;
t85 = 0;

for t = 1:tLength
	lastE = E;
	
	exciteV = reshape(W_E_1D * reshape(E, NE^2, 1), size(E));
	VE = W_EI * I + exciteV + input(:,:,t) + gammaE;
	VI = W_II * I + sum(sum(W_IE .* E)) + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
	
	diff = sum(sum(abs(lastE - E))) / sum(sum(E));
	
	if (t >= desiredMax85PercentRiseTime/dt)
		diffSum = diffSum + diff;
	end
	
	if (t85 == 0) && (min(min(E(goal > 0.01) ./ goal(goal > 0.01))) >= 0.85)
		t85 = t*dt;
	end

	if showsim == 1
		surf(ax,ax,E);
		zlim([0,1])
		title(sprintf('t = %f', t*dt));
		drawnow;
	end
end

error  = sqrt(mean(mean((E - goal).^2)));

% vel = rms(rms(-E + FE));
% exciteE = W_EE .* (-E + FE);
% exciteE = reshape(sum(sum(excite)), size(E));
% acc = rms(rms(0.5*(1 - tanh(VE).^2) .* (W_EI * (-I + FI) + exciteE) - (-E + FE)));

if (t85 > desiredMax85PercentRiseTime) || (t85 == 0) %|| (diffSum > desiredMaxAfterRiseDifference) || max(max(E)) > 1
	error = error * 1e6;
end
