function [X, error] = OptimiseGridCellAttractor(NE, stdev, dt)


tLength = 1000;

ax = linspace(0,1,NE+1);
ax = ax(1:end-1);

%% optimise stable packet
centre = ax(round(NE/2));
spikeLocation = [round(NE/2),round(NE/2)];
goal = circularnormpdf2(ax,ax,[centre,centre],[stdev,stdev],[1,1]);
goal = goal / max(goal(:));

desiredMax85PercentRiseTime = 0.5;
desiredMaxAfterRiseDifference = 1;

% x0 = [-1.4,-7.5,stdev,.5,-2,0.88,-4, 0.02, 0.005];
x0 =			[-1.7,	-10,	stdev,	.08,	-30,	0.88,	-4,		0.02,	0.005];
lowerBounds =	[-Inf,	-Inf,	1e-6,	0,		-Inf,	0,		-Inf,	1e-6,	1e-6];
upperBounds =	[0,		0,		Inf,	Inf,	0,		Inf,	0,		Inf,	Inf];
f = @(x) StableGridCellAttractor([x, x0(8:9)], goal, NE, dt, tLength, spikeLocation, desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0);

x = ga(f, 7, ...
	[],[],[],[], lowerBounds(1:7), upperBounds(1:7), [], optimoptions('ga', 'display', 'iter','useparallel',1,'populationsize', 200, 'maxGenerations', 50));

[error, e, t85, diffSum, vel, acc] = StableGridCellAttractor([x, x0(8:9)], goal, NE, dt, tLength, spikeLocation, ...
	desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0);

x = [x, x0(8:9)];

%% optimise velocity behaviour
tLength = round(2 / dt);
speed = 0.5;
ang = 0;

f = @(v) VelocityGridCellAttractor([x,v], NE, tLength, speed, ang, 0);

v = ga(f, 1, [],[],[],[],0,Inf, [], optimoptions('ga', 'display', 'iter','useparallel',1,'populationsize', 200, 'maxGenerations', 50));

x2 = [x, v];

error2 = VelocityGridCellAttractor(x2, NE, tLength, speed, ang, 0);
X = x2;
error = error2;