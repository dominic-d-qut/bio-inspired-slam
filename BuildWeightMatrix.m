function W = BuildWeightMatrix(NE, stddevConst, Wconst)
coder.inline('never')
	variance = (stddevConst / 360 * NE).^2;
% 	i = ones(NE,1) * (1:NE);
% 	j = (1:NE)' * ones(1,NE);
	[j,i] = meshgrid(1:NE);
% 	d_choices = cat(3,abs(j + NE - i), abs(i + NE - j), abs(j - i));
% 	d = min(d_choices, [], 3);
	d = reshape(min([abs(j(:) + NE - i(:)), abs(i(:) + NE - j(:)), abs(j(:) - i(:))],[],2),size(i));
	W = Wconst / sqrt(2*pi*variance) * exp(-d .* d / 2 / variance); % divided by 2 to correspond to normal distribution
% 	W =  * W ;%./(ones(NE,1) * sum(W));
end