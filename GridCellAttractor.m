% function [error, thetas, means, Es, goals] = robotSimulationVelocity(params, NE, tLength, targSigma, vvv, showsim)
% function [error] = robotSimulationVelocity(params, NE, tLength, targSigma, vvv, showsim)
function [E] = GridCellAttractor(params, NE, tLength, showsim)
% rng default

dt = 0.001; % s
IN = zeros(NE,NE,tLength);
tStart = 0.001;
tEnd = 0.2;
IN(5,5,tStart/dt:tEnd/dt) = 1;

% network params
tauE = 0.02; % time constant, excitatory neurons
tauI = 0.005; % time constant, inhibitory neuron
gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
weightV = 1;%params(8);
% weightL = params(9);
% stdevL = params(10);

E = zeros(NE,NE); % allocate space for NExNE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,NE); % I -> E weights
W_IE = weightIE * ones(NE,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix2d(NE, stddevConst, Wconst); % E -> E weights
W_Vx = BuildVelocityWeightMatrix2d(NE, 1, 0, weightV);
W_Vy = BuildVelocityWeightMatrix2d(NE, 0, 1, weightV);

% FOV = 15; % deg
% LTheta = [75, 130, 280, 330]; % deg

% lastMean = 0;
% thetas = zeros(1, tLength);
% means = zeros(1, tLength);
% vels = zeros(1, tLength);
% Es = zeros(NE, tLength);
% goals = zeros(NE, tLength);

error = 0;
lastX = 0;
lastY = 0;

for t = 1:tLength
	% update the robot theta
% 	if t > tStart/dt && t <= tEnd/dt
% 		vRot = vRobot(t);
% 	else
% 		vRot = 0;
% 	end
% 	theta = wrapTo360(theta + dt*vRot);
	
% 	L = zeros(NE,1);
	% can the robot see the landmark(s)
% 	bearings = wrapTo180(LTheta - theta);
% 	visibleLandmarks = abs(bearings) <= FOV;
% 	robotThetas = LTheta(visibleLandmarks) - bearings(visibleLandmarks);
% 	for l = 1:length(robotThetas)
% 		distr = normpdf(anglesDeg.', robotThetas(l), stdevL);
% 		distr = distr - sum(distr)/length(distr);
% 		distr = distr / max(distr);
% 		L = L + weightL * distr;
% 	end
	
	% update the network
% 	lastE = E;
% 	v = -vRot;% + 0.5*vRot*randn(); % noise!
% 	emat = W_V .* v + W_EE;
	if t*dt > 0.5
		velocity = 1.5;
		theta = 135;
	elseif t*dt > 0.2
		velocity = 0.4;
		theta = 0;
	else
		velocity = 0;
		theta = 135;
	end
	exciteMat = W_EE + -velocity*(cosd(theta) * W_Vx + sind(theta) * W_Vy);
	excite = exciteMat .* E;
	exciteV = reshape(sum(sum(excite)), size(E));
% 	exciteV = zeros(NE,NE);
% 	for i = 1:NE
% 		for j = 1:NE
% 			exciteV(i,j) = sum(sum(W_EE(:,:,i,j) .* E));
% 		end
% 	end
	VE = W_EI * I + exciteV + IN(:,:,t) + gammaE;
	VI = W_II * I + sum(sum(W_IE .* E)) + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);

	if showsim == 1
% 		surf(IN(:,:,t));
% 		hold on
		surf(0:NE-1,0:NE-1,E);
		zlim([0,1])
% 		imagesc(0:NE-1,0:NE-1,E, [0,1]);
% 		hold off
% 		legend('network', 'landmark input', 'actual direction', 'predicted direction', 'landmarks')
% 		hold off
		mean = circularMean2d(E);
		title(sprintf('state: mean = [%.2f, %.2f] cells, v = [%.2f, %.2f] cells/s',mean(1), mean(2), (mean(1)-lastX)/dt, (mean(2)-lastY)/dt));
		drawnow;
		lastX = mean(1);
		lastY = mean(2);
% 		f = gcf;
% 		frame = getframe(f);
% 		im = frame2im(frame);
% 		[imind,cm] = rgb2ind(im,256);
% 		if t == 5 
% 			 imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.05); 
% 		else 
% 			 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.05); 
% 		end
	end
end