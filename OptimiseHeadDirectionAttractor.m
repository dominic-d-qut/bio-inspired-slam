function [X, error] = OptimiseHeadDirectionAttractor(NE, stdev, dt)

%% evolve stability

% initial state and bounds
x0 =			[-1.5,	-7.5,	stdev,	6,		-8,		0.88,	-4,		0.02,	0.005];
lowerBounds =	[-Inf,	-Inf,	1e-6,	0,		-Inf,	0,		-Inf,	1e-6,	1e-6];
upperBounds =	[0,		0,		Inf,	Inf,	0,		Inf,	0,		Inf,	Inf];

fixedNeuronTimeConstants = true;
simulationTime = 1;
desiredMax85PercentRiseTime = 0.5;
desiredMaxAfterRiseDifference = 0.1;

tic;

% setup the goal
angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);
spikeLocation = round(NE/2);
spikeAngle = rad2deg(angles(spikeLocation));
goal = circularnormpdf(rad2deg(angles), spikeAngle, stdev, 360);
goal = goal ./ max(goal);
tLength = round(simulationTime / dt);

% Attempt 1 - fminsearch (quickest)
% if fixedNeuronTimeConstants
% 	x1 = fminsearch(@(x)RunStableAttractor([x,x0(8:9)], goal, NE, dt, tLength, spikeLocation, ...
% 		desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0), x0(1:7), optimset('display', 'iter'));
% 	inBounds = all((x1 >= lowerBounds(1:7)) & (x1 <= upperBounds(1:7)));
% 	x1 = [x1,x0(8:9)];
% else
% 	x1 = fminsearch(@(x)RunStableAttractor(x, goal, NE, dt, tLength, spikeLocation, ...
% 		desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0), x0, optimset('display', 'iter'));
% 	inBounds = all((x1 >= lowerBounds) & (x1 <= upperBounds));
% end

% [error, e, t85, diffSum, vel, acc] = RunStableAttractor(x1, goal, NE, dt, tLength, spikeLocation, ...
% 	desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0);

% Attempt 2 - fmincon (constrains the result)
% if ~inBounds
% 	disp('Quick minimisation search returned out of bounds solution - likely to be unstable. Running again with contraints.');
% 	if fixedNeuronTimeConstants
% 		x1 = fmincon(@(x)RunStableAttractor([x,x0(8:9)], goal, NE, dt, tLength, spikeLocation, ...
% 			desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0), x0(1:7), ...
% 			[],[],[],[], lowerBounds(1:7), upperBounds(1:7), [], optimset('display', 'iter'));
% 		x1 = [x1,x0(8:9)];
% 	else
% 		x1 = fmincon(@(x)RunStableAttractor(x, goal, NE, dt, tLength, spikeLocation, ...
% 			desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0), x0, ...
% 			[],[],[],[], lowerBounds, upperBounds, [], optimset('display', 'iter'));
% 	end
% end

% [error, e, t85, diffSum, vel, acc] = RunStableAttractor(x1, goal, NE, dt, tLength, spikeLocation, ...
% 	desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0);

% Attempt 3 - Genetic Algorithm (if the conditions are very different so the previous approaches don't find a solution)
% if error > 0.1
% 	disp('Error seems large - attempting Genetic Algorithm Optimisation');

% always use the GA approach
	if fixedNeuronTimeConstants
		x1 = ga(@(x)RunStableAttractor([x,x0(8:9)], goal, NE, dt, tLength, spikeLocation, ...
			desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0), 7, [],[],[],[],...
			lowerBounds(1:7), upperBounds(1:7), [], optimoptions('ga','display', 'iter','useparallel',1,'populationsize', 200, 'maxGenerations', 50));
		x1 = [x1,x0(8:9)];
	else
		x1 = ga(@(x)RunStableAttractor(x, goal, NE, dt, tLength, spikeLocation, ...
			desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0), 9, [],[],[],[],...
			lowerBounds, upperBounds, [], optimoptions('ga','display', 'iter','useparallel',1,'populationsize', 200, 'maxGenerations', 50));
	end
% end

[error, e, t85, diffSum, vel, acc] = RunStableAttractor(x1, goal, NE, dt, tLength, spikeLocation, ...
	desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, 0);

%% setup velocity goal matrix
tLength = round(2 / dt);
speed = 250; %target speed to train for in degrees per second

input = zeros(1, tLength);

tStart = round(0.5 / dt);
tEnd = round((tLength*dt - 0.2) / dt);
input(tStart:tEnd) = speed;

%% evolve velocity

y = ga(@(y)robotSimulationVelocity([x1,y,0,1], NE, dt, tLength, input, 0), 1, [],[],[],[], ...
	0, Inf, [], optimoptions('ga', 'display', 'iter','useparallel',1,'populationsize', 200, 'maxGenerations', 50));

x2 = [x1, y];
error2 = robotSimulationVelocity([x2,0,1], NE, dt, tLength, input, 0);

X = x2;
error = error2;
