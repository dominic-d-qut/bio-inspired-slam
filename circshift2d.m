function M = circshift2d(A, dx, dy)
	if dx ~= 0
		if dx ~= floor(dx)
			A = (ceil(dx) - dx) * circshift(A, floor(dx), 2) + (dx - floor(dx)) * circshift(A, ceil(dx), 2);
		else
			A = circshift(A, dx, 2);
		end
	end
	if dy ~= 0
		if dy ~= floor(dy)
			A = (ceil(dy) - dy) * circshift(A, floor(dy), 1) + (dy - floor(dy)) * circshift(A, ceil(dy), 1);
		else
			A = circshift(A, dy, 2);
		end
	end
	M = A;
end