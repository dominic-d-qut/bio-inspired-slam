function M = circshift1d(A, d)
	if d ~= 0
		if d ~= floor(d)
			A = (ceil(d) - d) * circshift(A, floor(d)) + (d - floor(d)) * circshift(A, ceil(d));
		else
			A = circshift(A, d, 2);
		end
	end
	M = A;
end