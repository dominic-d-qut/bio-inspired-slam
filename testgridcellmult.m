clear variables

NE = 10;

A = 0.1*ones(NE, NE, NE, NE);
B = magic(NE);

for i = 1:NE
	for j = 1:NE
		A(i,j,i,j) = 2;
	end
end

A_g = gpuArray(A);
B_g = gpuArray(B);

%% 1. bsxfun
tic;
% C = bsxfun(@times, A, B);
C = A .* B;
D = reshape(sum(sum(C)), NE, NE);
toc;

%% 2. for loop
tic;
D2 = zeros(NE, NE);
for i = 1:NE
	for j = 1:NE
		D2(i,j) = sum(sum(A(:,:,i,j) .* B));
	end
end
toc;

areEqual = all(all(D2 - D < 1e-6));

%% 3. gpuArray
tic;
C3 = pagefun(@times, A_g, B_g);
D3 = reshape(sum(sum(C3)), NE, NE);
toc;