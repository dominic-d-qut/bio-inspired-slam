% function [error, thetas, means, Es, goals] = robotSimulationVelocity(params, NE, tLength, targSigma, vvv, showsim)
% function [error] = robotSimulationVelocity(params, NE, tLength, targSigma, vvv, showsim)
function [error, thetas] = robotSimulationVelocity(params, NE, dt, tLength, vRot, showsim)

IN = zeros(NE,tLength);
IN(1, 1:round(0.1/dt)) = 1; % set initial position to 0 deg
angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);
anglesSin = sin(angles);
anglesCos = cos(angles);
anglesDeg = rad2deg(angles);

% robot state
theta = 0; % deg

tStartRot = find(vRot > 0, 1)*dt;
tEndRot = find(vRot > 0, 1, 'last')*dt;

% network params
gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
tauE = params(8); % time constant, excitatory neurons
tauI = params(9); % time constant, inhibitory neuron
weightV = params(10);
weightL = params(11);
stdevL = params(12);
% if stdevL <= 0
% 	stdevL = 0.01;
% 	error = 1000000;
% 	return;
% end

E = zeros(NE,1); % allocate space for NE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,1); % I -> E weights
W_IE = weightIE * ones(1,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix(NE, stddevConst, Wconst).'; % E -> E weights
W_V = BuildVelocityWeightMatrix(NE, 1, weightV);

FOV = 15; % deg
LTheta = [20, 50, 75, 120, 170, 220, 280, 315]; % deg
landmarkEstimates = zeros(NE, length(LTheta));
timesLandmarkSeen = zeros(1, length(LTheta));
thetas = zeros(2,tLength);

lastMeanAngle = 0;
lastVel = 0;
% thetas = zeros(1, tLength);
% means = zeros(1, tLength);
% vels = zeros(1, tLength);
% Es = zeros(NE, tLength);
% goals = zeros(NE, tLength);

error = 0;
firstframe = 1;
sumDeltaX = 0;

for t = 1:tLength
	% update the robot theta
	theta = wrapTo360(theta + dt*vRot(t));
	thetas(1,t) = theta;
	
	L = zeros(NE,1);
	% can the robot see the landmark(s)
	bearings = wrapTo180(LTheta - theta);
	visibleLandmarks = abs(bearings) <= FOV;
	robotThetas = LTheta(visibleLandmarks) - bearings(visibleLandmarks);
	for l = 1:length(LTheta)
		if visibleLandmarks(l)
			estimatedLandmarkAngle = circshift1d(E, (bearings(l)) * (NE / 360) );
			timesLandmarkSeen(l) = timesLandmarkSeen(l) + 1;
			
% 			if timesLandmarkSeen(l) == 1
				landmarkEstimates(:,l) = (1/timesLandmarkSeen(l)) * estimatedLandmarkAngle + ((timesLandmarkSeen(l)-1)/timesLandmarkSeen(l)) * landmarkEstimates(:,l);
% 			end
			
			estimatedRobotAngle = fastCircularMean(anglesCos, anglesSin,landmarkEstimates(:,l)) - bearings(l);

			distr = circularnormpdf(anglesDeg.', estimatedRobotAngle, stdevL, 360);
			if sum(distr) ~= 0
				distr = distr - sum(distr)/length(distr);
% 				distr = distr / max(distr);
			else
				[~,I] = min(abs(anglesDeg - estimatedRobotAngle));
				distr(I) = 1;				
			end
			L = L + weightL * distr;
		end
	end
	
	% update the network
% 	lastE = E;
	v = -vRot(t);% + 0.5*vRot*randn(); % noise!
	emat = W_V .* v + W_EE;
	VE = W_EI * I + emat * E + IN(:,t) + L + gammaE;
	VI = W_II * I + W_IE * E + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
	
% 	diff = sum(abs(lastE - E)) / sum(E);
% 	[mean, stdev] = circularStats(E);
	meanAngle = fastCircularMean(anglesCos, anglesSin, E);
	deltaX = wrapTo180(meanAngle - lastMeanAngle);
% 	deltaV = deltaX / dt;

	if t >= tStartRot/dt		
		error = error + abs(wrapTo180(meanAngle - lastMeanAngle) - dt*vRot(t));
% 		error = error + sqrt(mean((E - goal).^2));
% 		error = error + abs(wrapTo180(mean - theta)) / 180;
	end
% 	deltaVs(t) = deltaV;
	if t > tStartRot/dt
		sumDeltaX = sumDeltaX + deltaX;
	end
	lastVel = meanAngle - lastMeanAngle;
	lastMeanAngle = meanAngle;
	
	thetas(2,t) = meanAngle;

	if showsim == 1 && mod(t, 10) == 0 %&& t >= tStartRot/dt
% 		bar(rad2deg(angles), E);
% 		xlim([0,360]);
% 		ylim([0,1]);
% 		xlabel('Head Direction (\circ)')
% 		ylabel('Cell Firing Rate (normalised)')
% 		title('Head Direction Network Response')
		set(gcf, 'color', 'w');
% 		set(gca, 'tickdir', 'out');
% 		xticks(0:45:360);
% 		box off;
		polarplot(deg2rad(LTheta), 0.9*ones(1, length(LTheta)), 'ko','markersize', 10, 'markerfacecolor', 'black');
		hold on;
		polarplot([deg2rad(fastCircularMean(anglesCos, anglesSin, landmarkEstimates(:,timesLandmarkSeen ~= 0))); deg2rad(LTheta(timesLandmarkSeen ~= 0))], ...
			[0.9*ones(1, nnz(timesLandmarkSeen ~= 0)); 0.9*ones(1, nnz(timesLandmarkSeen ~= 0))], 'm-', 'linewidth', 1.5);
		polarplot(deg2rad(fastCircularMean(anglesCos, anglesSin, landmarkEstimates(:,timesLandmarkSeen ~= 0))), 0.9*ones(1, nnz(timesLandmarkSeen ~= 0)), 'mx','markersize', 15, 'markerfacecolor', 'black');
		polarplot([angles,angles(1)], [E;E(1)], 'b-o', 'markersize', 5);
% 		polarplot([angles,angles(1)], [goal;goal(1)], 'g');
		rlim([0,1]);
% 		ylim([0 1])
% 		xlim([0,360]);
% 		polarplot(angles, goal, 'g');
% 		stem(rad2deg(angles), IN(:,t), 'r');
		polarplot([deg2rad(meanAngle),deg2rad(meanAngle)], [0,1], 'r', 'linewidth', 2);
		polarplot([deg2rad(theta),deg2rad(theta)], [0,1], 'm', 'linewidth', 2);
		
% 		legend('landmarks', 'network', 'network direction', 'true direction');
		
% 		polarplot(angles, L, 'k');
		
% 		legend('network', 'landmark input', 'actual direction', 'predicted direction', 'landmarks')
		hold off
% 		title(sprintf('state: mean = %.1f, std = %.1f, v = %.1f',mean,stdev,deltaV));
		drawnow;
% 		if firstframe == 1
% 			vid = VideoWriter('rotation.mp4','MPEG-4');
% 			vid.FrameRate = 30;
% 			open(vid);
% % 			imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.05); 
% 			firstframe = 0;
% 		else 
% % 			 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.05); 
% 		end
% 		f = gcf;
% 		frame = getframe(f);
% 		writeVideo(vid,frame);
% 		im = frame2im(frame);
% 		[imind,cm] = rgb2ind(im,256);
		
	end
end
% close(vid);
avgV = sumDeltaX / (tEndRot - tStartRot);
error = error / (tEndRot - tStartRot) * dt;