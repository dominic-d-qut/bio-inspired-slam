function W = BuildWeightMatrix3d(NE, stdev, Wconst)
	xDim = NE(1); 
	yDim = NE(2);
	thetaDim = NE(3);
	
	xStdev = stdev(1); % grids
	yStdev = stdev(2); % grids
	thetaStdev = stdev(3); % rad
	
	W = zeros(xDim, yDim, thetaDim, xDim, yDim, thetaDim);
	[i,j] = meshgrid(1:xDim,1:yDim);
	k = linspace(0, 2*pi, thetaDim + 1);
	k = k(1:end - 1);
	for ii = 1:xDim
		for jj = 1:yDim
			for kk = 1:thetaDim
				dx = ii - i;
				dy = jj - j;
				dt = k(kk) - k;
				mindx = reshape(min([abs(dx(:)), abs(NE + dx(:)), abs(NE - dx(:))], [], 2), size(dx)) / NE(1) / 2 / xStdev;
				mindy = reshape(min([abs(dy(:)), abs(NE + dy(:)), abs(NE - dy(:))], [], 2), size(dy)) / NE(2) / 2 / yStdev;
				mindt = min([abs(dt); abs(2*pi + dt); abs(2*pi - dt)]) / 2 / thetaStdev;
				d2 = mindx.^2 + mindy.^2;
				for kk2 = 1:thetaDim
					W(:,:,kk2,ii,jj,kk) = (mindt(kk2)^2 + d2);
				end
			end
		end
	end
 	W = Wconst / ((2*pi)^(3/2)*(xStdev * yStdev * thetaStdev)) * exp(-W); % not sure about scaling factor
end