function W_V = BuildVelocityWeightMatrix(NE, v, weightV)
	if v ~= floor(v)
		W_Vpos = weightV*((ceil(v) - v)*circshift(eye(NE),floor(v),2) + (v - floor(v))*circshift(eye(NE),ceil(v),2));
		W_Vneg = -weightV*((ceil(v) - v)*circshift(eye(NE),-floor(v),2) + (v - floor(v))*circshift(eye(NE),-ceil(v),2));
		W_V = W_Vpos + W_Vneg;
	else
		W_Vpos = weightV*circshift(eye(NE),v,2);
		W_Vneg = -weightV*circshift(eye(NE),-v,2);
		W_V = W_Vpos + W_Vneg;
	end
end