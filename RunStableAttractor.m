% function error = RunStableAttractor(params, input, goalMean, goalStdev, NE, dt, showsim)
function [error, E, t85, diffSum, vel, acc] = RunStableAttractor(params, goal, NE, dt, tLength, spikeLocation, ...
	desiredMax85PercentRiseTime, desiredMaxAfterRiseDifference, showsim)

gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons excite the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
tauE = params(8); % time constant, excitatory neurons
tauI = params(9); % time constant, inhibitory neuron

E = zeros(NE,1); % allocate space for NE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,1); % I -> E weights
W_IE = weightIE * ones(1,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix(NE, stddevConst, Wconst); % E -> E weights


tStart = dt;
tEnd = dt * round(0.1 / dt); % make sure this works for all dt

input = zeros(NE,NE,tLength);
input(spikeLocation,tStart/dt:tEnd/dt) = 1;

angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);
t85 = 0;
diffSum = 0;
for t = 1:tLength
% while (t <= tLength) %&& (t <= tEnd/dt || diff > 1e-5)
	lastE = E;
	
	VE = W_EI * I + W_EE * E + gammaE + input(:,t);
	VI = W_II * I + W_IE * E + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
% 	Edotdot = (0.5*(1 - tanh(VE).^2) .* (W_EI * (-I + FI) + W_EE * (-E + FE)) - (-E + FE));
% 	Idotdot = (0.5*(1 - tanh(VI).^2) .* (W_II * (-I + FI) + W_IE * (-E + FE)) - (-I + FI));
	diff = sum(abs(lastE - E)) / sum(E);
% 	VEdot = W_EI * (-I + FI) + W_EE * (-E + FE);
% 	newVE = W_EI * I + W_EE * E + gammaE + input(:,t);
	
	if (t >= desiredMax85PercentRiseTime/dt)
		diffSum = diffSum + diff;
	end
	
% 	if (t85 == 0) && ( min(E([spikeLocationMinus1,spikeLocation,spikeLocationPlus1]).' ./ ...
% 			goal([spikeLocationMinus1,spikeLocation,spikeLocationPlus1])) >= 0.85)
	if (t85 == 0) && (min(E(goal > 0.01).' ./ goal(goal > 0.01)) >= 0.85)
		t85 = t*dt;
	end
	
	if showsim
% 		subplot(2,1,1)
		stem(rad2deg(angles), E, 'linewidth', 1.5, 'markersize', 7);
		hold on
		plot(rad2deg(angles), goal, 'g', 'linewidth', 1.5);
		hold off
		ylim([0 1])
		xlim([0,360]);
		xticks(0:45:360);
		set(gcf, 'color', 'w');
		set(gca, 'tickdir', 'out');
		box off;
		xlabel('Head Direction (\circ)')
		ylabel('Cell Firing Rate (normalised)')
		title(sprintf('Head Direction Network Response %f s', t*dt))
% 		subplot(2,1,2)
% 		plot(rad2deg(angles), Edotdot, 'k', 'linewidth', 1);
% 		hold on
% 		plot(rad2deg(angles), ones(size(angles))*Idotdot, 'r', 'linewidth', 1);
% 		hold off
% 		ylim([-1 1])
% 		xlim([0,360]);
% 		xticks(0:45:360);
% 		set(gcf, 'color', 'w');
% 		set(gca, 'tickdir', 'out');
% 		box off;
% 		title(sprintf('biggest pos = %f, biggest neg = %f',max(Edotdot(Edotdot>0)),min(Edotdot(Edotdot<0))))
% 		hold on
% 		stem(rad2deg(angles), input(:,t), 'r');
% 		hold off
% 		title('response of network to 0.1s spike with 0.9s to settle - stdev = 10 degrees');
		drawnow;
% 		f = gcf;
% 		frame = getframe(f);
% 		im = frame2im(frame);
% 		[imind,cm] = rgb2ind(im,256);
% 		if t == 1 
% 			 imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.05); 
% 		else 
% 			 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.05); 
% 		end 
	end
end

error = sqrt(mean((E.' - goal).^2));
vel = rms(-E + FE);
acc = rms(0.5*(1 - tanh(VE).^2) .* (W_EI * (-I + FI) + W_EE * (-E + FE)) - (-E + FE));

if (t85 > desiredMax85PercentRiseTime) || (t85 == 0) || (diffSum > desiredMaxAfterRiseDifference) || max(E) > 1
	error = error * 1e6;
end

end