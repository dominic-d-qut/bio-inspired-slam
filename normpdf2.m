function z = normpdf2(xRange, yRange, mu, sigma);

	[x,y] = meshgrid(xRange, yRange);
	z = exp(-0.5 * (((x - mu(1))./sigma(1)).^2 + ((y - mu(2))./sigma(2)).^2)) ./ (2*pi .* sigma(1) .* sigma(2));