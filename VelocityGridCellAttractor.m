function error = VelocityGridCellAttractor(params, NE, tLength, v, vTheta, showsim)

dt = 0.001; % s
IN = zeros(NE,NE,tLength);

ax = linspace(0,1,NE+1);
ax = ax(1:end-1);

tStart = 0.5;
tEnd = tLength*dt - 0.2;
IN(round(0.5*NE),round(0.5*NE),1:0.1/dt) = 1;
% IN(round(0.2*NE),round(0.5*NE),tEnd/2/dt+1:tEnd/dt) = 2;

% network params
gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
tauE = params(8); % time constant, excitatory neurons
tauI = params(9); % time constant, inhibitory neuron
weightV = params(10);

E = zeros(NE,NE); % allocate space for NExNE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,NE); % I -> E weights
W_IE = weightIE * ones(NE,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix2d(NE, stddevConst, Wconst); % E -> E weights
W_Vx = BuildVelocityWeightMatrix2d(NE, 1, 0, weightV);
W_Vy = BuildVelocityWeightMatrix2d(NE, 0, 1, weightV);

error = 0;
lastX = 0;
lastY = 0;

vXTotal = 0;
vYTotal = 0;

firstframe = 1;

for t = 1:tLength
	if t*dt > tStart && t*dt <= tEnd
		velocity = v;
		theta = vTheta;%0;
% 	elseif t*dt > 0.5 && t*dt <= 0.74
% 		velocity = v;
% 		theta = 180;
% 	elseif t*dt > 0.8
% 		velocity = v;
% 		theta = -90+2*(t-0.8/dt);
	else
		velocity = 0;
		theta = 0;
	end
	
	exciteMat = W_EE + -velocity*(cosd(theta) * W_Vx + sind(theta) * W_Vy);
	excite = exciteMat .* E;
	exciteV = reshape(sum(sum(excite)), size(E));
	VE = W_EI * I + exciteV + IN(:,:,t) + gammaE;
	VI = W_II * I + sum(sum(W_IE .* E)) + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
	
	mean = circularMean2d(E);
	
	vX = (mod((mean(1)-lastX)+.5,1)-.5) / dt;
	vY = (mod((mean(2)-lastY)+.5,1)-.5) / dt;
	
	if t*dt > tStart
		vXTotal = vXTotal + vX*dt;
		vYTotal = vYTotal + vY*dt;
		error = error + sqrt((vX - velocity*cosd(theta)).^2 + (vY - velocity*sind(theta)).^2);
	end

	if showsim == 1 % && t*dt > 0.2
		ax = linspace(0, 1, NE+1);
		ax = ax(1:end-1);
% 		imagesc(ax+ax(2)/2, ax+ax(2)/2, E, [0,1]);
		surf(ax, ax, E);
		zlim([0,1]);
		set(gcf, 'color', 'w');
		xlabel('x')
		ylabel('y')
		title(sprintf('Grid Cell Activity: V = [%.1f,%.1f], actual = [%.1f, %.1f]',vX,vY,velocity*cosd(theta),velocity*sind(theta)))
% 		colorbar;
		set(gca,'Ydir','Normal')
% 		title(sprintf('state: mean = [%.2f, %.2f] grid, v = [%.2f, %.2f] grid/s',mean(1), mean(2), vX, vY));
		drawnow;
% 		figure
% 		surf(ax, ax, E);
% 		set(gcf, 'color', 'w');
% 		xlabel('x')
% 		ylabel('y')
% 		return
% 		frame = getframe(gcf);
% 		writeVideo(vid,frame);
% 		if firstframe == 1
% 			vid = VideoWriter('gridcells.mp4','MPEG-4');
% 			vid.FrameRate = 30;
% 			open(vid);
% 			firstframe = 0;
% 		end
% 		f = gcf;
% 		frame = getframe(f);
% 		writeVideo(vid,frame);
	end
	
	lastX = mean(1);
	lastY = mean(2);
end
error = error / (tEnd - tStart);
v = [vXTotal, vYTotal] / (tEnd - tStart);
% close(vid)
