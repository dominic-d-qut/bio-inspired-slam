close all;
f = figure
set(f, 'position', [419,299,560,280])
subplot(2,4,[1,2,5,6])
plot(x1, y1, landmarks1(1,:), landmarks1(2,:), 'k.', 'markersize', 40, 'linewidth', 1.5);
hold on
plot(x1(1), y1(1), 'rx', 'markersize', 15);
hold off
xlim([-.1,1.1])
axis equal
legend('path', 'landmarks', 'start')
title('Figure 8 environment')
subplot(2,4,[3,4,7,8])
plot(x2, y2, landmarks2(1,:), landmarks2(2,:), 'k.', 'markersize', 40, 'linewidth', 1.5);
hold on
plot(x2(1), y2(1), 'rx', 'markersize', 15);
hold off
xlim([-.1,1.1])
axis equal
title('Office environment')
legend('path', 'landmarks', 'start')