function y = tanhActivation(x)
% 	y = (x);
% 	y (x < 0) = 0.01 * x(x < 0);
	y = 0.5 + 0.5*tanh(x);
end

% x = -5:0.001:5;
% figure
% subplot(2,2,1)
% plot(x, 0.5 + 0.5*tanh(x), 'linewidth', 1.5);
% title('Tanh')
% box off
% grid on
% ylim([-0.1,1.1])
% subplot(2,2,2)
% plot(x, (0.5 + x) .* (x > 0), 'linewidth', 1.5);
% title('ReLU')
% box off
% grid on
% ylim([-0.1,5.1])
% subplot(2,2,3)
% plot(x, (x) .* (x > 0) + (0.01 * x) .* (x <= 0), 'linewidth', 1.5);
% title('Leaky ReLU')
% box off
% grid on
% ylim([-0.1,5.1])
% subplot(2,2,4)
% plot(x, (log(1+x)) .* (x > 0), 'linewidth', 1.5);
% title('Log')
% box off
% grid on
% ylim([-0.1,2])