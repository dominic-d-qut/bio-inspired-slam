function M = circshift2dAngleDistr(xyDistribution, angleDistribution, range)
	angles = linspace(0, 2*pi, length(angleDistribution)+1);
	angles = angles(1:end-1);
	
	if sum(angleDistribution) ~= 0
		normAngleDistribution = angleDistribution ./ sum(angleDistribution);
	else
		normAngleDistribution = angleDistribution;
	end
	
	M = zeros(size(xyDistribution));
	
	for i = 1:length(normAngleDistribution)
		M = M + normAngleDistribution(i) * circshift2d(xyDistribution, range*cos(angles(i)), range*sin(angles(i)));
	end
end