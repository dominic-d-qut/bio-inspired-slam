function mean = fastCircularMean(anglesCos, anglesSin, distribution)
coder.inline('never')
	x = anglesCos * distribution;
	y = anglesSin * distribution;
	mean = atan2d(y, x);
end