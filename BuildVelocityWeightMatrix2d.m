function W_V = BuildVelocityWeightMatrix2d(NE, offsetX, offsetY, weightV)
	W_V = zeros(NE,NE,NE,NE);

	for ii = 1:NE
		for jj = 1:NE
			wv = zeros(NE,NE);
			wv(ii,jj) = 1;

			W_Vpos = wv;
			W_Vneg = wv;

			if offsetX ~= 0
				if offsetX ~= floor(offsetX)
					W_Vpos = weightV*((ceil(offsetX) - offsetX)*circshift(W_Vpos,floor(offsetX),2) + (offsetX - floor(offsetX))*circshift(W_Vpos,ceil(offsetX),2));
					W_Vneg = weightV*((ceil(offsetX) - offsetX)*circshift(W_Vneg,-floor(offsetX),2) + (offsetX - floor(offsetX))*circshift(W_Vneg,-ceil(offsetX),2));
				else
					W_Vpos = weightV*circshift(W_Vpos,offsetX,2);
					W_Vneg = weightV*circshift(W_Vneg,-offsetX,2);
				end
			end

			if offsetY ~= 0
				if offsetY ~= floor(offsetY)
					W_Vpos = weightV*((ceil(offsetY) - offsetY)*circshift(W_Vpos,floor(offsetY),1) + (offsetY - floor(offsetY))*circshift(W_Vpos,ceil(offsetY),1));
					W_Vneg = weightV*((ceil(offsetY) - offsetY)*circshift(W_Vneg,-floor(offsetY),1) + (offsetY - floor(offsetY))*circshift(W_Vneg,-ceil(offsetY),1));
				else
					W_Vpos = weightV*circshift(W_Vpos,offsetY,1);
					W_Vneg = weightV*circshift(W_Vneg,-offsetY,1);
				end
			end

			W_V(:,:,ii,jj) = W_Vpos - W_Vneg;
		end
	end
end