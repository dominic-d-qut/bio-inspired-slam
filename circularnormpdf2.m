function y = circularnormpdf2(xRange, yRange, mu, sigma, max)
	[x,y] = meshgrid(xRange, yRange);
	diffX = x - mu(1);
	diffY = y - mu(2);
	minDiffX = reshape(min([abs(diffX(:)), abs(diffX(:) + max(1)), abs(diffX(:) - max(1))], [], 2), size(diffX));
	minDiffY = reshape(min([abs(diffY(:)), abs(diffY(:) + max(2)), abs(diffY(:) - max(2))], [], 2), size(diffY));
	z2 = minDiffX.^2 / sigma(1)^2 + minDiffY.^2 / sigma(2)^2;
	y = 1 ./ (2*pi*sigma(1)*sigma(2)) * exp(-0.5 * z2);