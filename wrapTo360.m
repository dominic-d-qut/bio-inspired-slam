function wrappedAngle = wrapTo360(angle)
% 	angle(abs(angle) > 180) = mod(angle(abs(angle) > 180)+180, 360) - 180;
% 	wrappedAngle = angle;
	wrappedAngle = mod(angle, 360);
	
% 	if angle > 0
% 		wrappedAngle = mod(angle, 360);
% 		if wrappedAngle == 0
% 			wrappedAngle = 360;
% 		end
% 	else
% 		wrappedAngle = mod(angle, 360);
% 	end
end