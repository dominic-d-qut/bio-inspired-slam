% function error = RunVelocityAttractor(params, input, goalMean, goalStdev, NE, dt, showsim)
function [avgV, deltaVs, stdev] = RunVelocityAttractor(params, input, v, NE, dt, tStart, tEnd, showsim, weightV)

% tauE = params(1); % time constant, excitatory neurons
% gammaE = params(2); % tonic inhibition, excitatory neurons
% tauI = params(3); % time constant, inhibitory neuron
% gammaI = params(4); % tonic inhibition, inhibitory neuron
% stddevConst = params(5); % intra-ring weighting field width (degrees)
% Wconst = params(6); % intra-ring weighting field strength
% weightEI = params(7); % How much does the inhibitory neuron inhibit all others
% weightIE = params(8); % How much do all the excitory neurons inhibit the inhibitory one
% weightII = params(9); % How much does the inhibitory neuron excite itself

tauE = 0.02; % time constant, excitatory neurons
tauI = 0.005; % time constant, inhibitory neuron
gammaE = params(1); % tonic inhibition, excitatory neurons
gammaI = params(2); % tonic inhibition, inhibitory neuron
stddevConst = params(3); % intra-ring weighting field width (degrees)
Wconst = params(4); % intra-ring weighting field strength
weightEI = params(5); % How much does the inhibitory neuron inhibit all others
weightIE = params(6); % How much do all the excitory neurons inhibit the inhibitory one
weightII = params(7); % How much does the inhibitory neuron excite itself
% weightV = 2.5;

E = zeros(NE,1); % allocate space for NE excitatory neurons
I = 0; % one inhibitory neuron
W_EI = weightEI * ones(NE,1); % I -> E weights
W_IE = weightIE * ones(1,NE); % E -> I weights
W_II = weightII; % I -> I weights
W_EE = BuildWeightMatrix(NE, stddevConst, Wconst); % E -> E weights
W_V = BuildVelocityWeightMatrix(NE, -v, weightV);

inputSteps = size(input,2);
angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);
t = 1;
n = 1;
diff = 1;
lastMean = rad2deg(angles(input(:,1) == 1));
sumDeltaX = 0;
deltaVs = zeros(1,inputSteps);

if showsim == 2
	figure('position', [50,50,800,600]);
end

for t = 1:inputSteps
% while (t < inputSteps || diff > 1e-5) && (n <= 500)
	lastE = E;
	
	if t > tStart/dt && t <= tEnd/dt
		VE = W_EI * I + W_EE * E + gammaE + input(:,t) + W_V * E;
	else
		VE = W_EI * I + W_EE * E + gammaE + input(:,t);
	end
	VI = W_II * I + W_IE * E + gammaI;
	FE = tanhActivation(VE); % activation function
	FI = tanhActivation(VI);
	E = E + dt/tauE * (-E + FE);
	I = I + dt/tauI * (-I + FI);
	
	
	diff = sum(abs(lastE - E)) / sum(E);
	[mean, stdev] = circularStats(E);
	deltaX = wrapTo180(mean - lastMean);
	deltaV = deltaX / dt;
	deltaVs(t) = deltaV;
	if t > tStart/dt
		sumDeltaX = sumDeltaX + deltaX;
	end
	lastMean = mean;
	
	if showsim == 1
		stem(rad2deg(angles), E, 'b');
		ylim([0 1])
		xlim([0,360]);
		hold on
		stem(rad2deg(angles), input(:,t), 'r');
		hold off
		title(sprintf('state: mean = %.1f, std = %.1f, v = %.1f',mean,stdev,deltaV));
		drawnow;
	elseif showsim == 2 && mod(t, 1) == 0
% 		suptitle('Attractor: stddev = 10 \circ, copy distance = 0.5, copy weight = 50');
		subplot(2,2,1);
		stem(rad2deg(angles), E, 'b');
		ylim([0 1])
		xlim([0,360]);
		hold on
		stem(rad2deg(angles), input(:,t), 'r');
		hold off
		title('state')
		subplot(2,2,2);
		stem(rad2deg(angles), W_V * E, 'b');
		xlim([0,360]);
		ylim([-weightV,weightV])
		title('velocity copy');
		subplot(2,2,3);
		polarplot(angles, E, 'b');
		rlim([0,1]);
		title('direction');
		subplot(2,2,4);
		cla;
		axis off;
		ylim([0.2,1.6])
		xlim([-0.2,1])
		hold on
		text(0,1,sprintf('mean = %.1f degrees',mean));
		text(0,0.85,sprintf('std = %.1f degrees',stdev));
		text(0,0.7,sprintf('v = %.1f degrees/s',deltaV));
		hold off
		drawnow;
		f = gcf;
		frame = getframe(f);
		im = frame2im(frame);
		[imind,cm] = rgb2ind(im,256);
		if t == 1 
			 imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.05); 
		else 
			 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.05); 
		end 
	end
	
	if t < inputSteps
		t = t + 1;
	end
	n = n + 1;
end

initialPos = rad2deg(angles(find(input(:,1) == 1,1)));
avgV = sumDeltaX / (200 * dt);

if showsim == 3
	plot(0:dt:inputSteps*dt-dt,deltaVs);
	hold on
	plot([0.2,0.4], [avgV,avgV], 'r--');
	hold off
	title(sprintf('velocity over time - velocity starts at t = %.1f s and ends at t = %.1f s',tStart,tEnd))
	ylabel('Approximate network velocity (\circ/s)');
	xlabel('time, t (s)');
	legend('estimated velocity (\Delta position)', 'average velocity', 'location', 'east');
end

end





