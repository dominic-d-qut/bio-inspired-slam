NE = 100;
stdev = 20;
dt = 0.001;

%% evolve separately
angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);
IN = zeros(NE,200);
spikeLocation = 1;
spikeAngle = rad2deg(angles(spikeLocation));
IN(spikeLocation,1:100) = 1;
goal = circularnormpdf(rad2deg(angles), spikeAngle, stdev, 360);
goal = goal ./ max(goal);
x0 = [-1.5,-7.5,stdev,6,-8,0.88,-4];
tic
x = fminunc(@(x)RunStableAttractor(x, IN, goal, NE, dt, 0), x0, optimset('display', 'iter'));
toc
%%
tic

NT = 2000;

goals = zeros(NE, NT);
theta = 0;
anglesDeg = rad2deg(angles);
for t = 1:NT
	% update the robot theta
	if t > 0.2/dt && t <= (NT*dt - 0.2)/dt
		vRot = 10;
	else
		vRot = 0;
	end
	theta = wrapTo360(theta + dt*vRot);
	d = circularnormpdf(anglesDeg, theta, stdev, 360).';
	goals(:,t) = d / max(d);
end

% y = fminsearch(@(y)robotSimulationVelocity([x,y,0], NE, NT, goals, 100, 0), [0], optimset('display', 'iter'));
% goal = circularnormpf(rad2deg(angles))
% y = fminunc(@(y)robotSimulationVelocity([x,y], NE, NT, goals, 100, 0), [0.014,0.1], optimset('display', 'iter'));
% y = fmincon(@(y)robotSimulationVelocity([x,y,0], NE, NT, goals, 100, 0), [0.014], [],[],[],[], 0, Inf, [], optimset('display', 'iter'));
y = patternsearch(@(y)robotSimulationVelocity([x,y,0], NE, NT, goals, 100, 0), [0], [], [], [], [], 0, 0.1, [], optimset('display', 'iter'));
% obj = @(y)robotSimulationVelocity([x,y,0], NE, NT, goals, 100, 0);
% y = [0];
% problem = createOptimProblem('fmincon','objective',obj,'x0',y, 'options', optimset('display', 'iter'));
% gs = GlobalSearch;
% [y,fg,flg,og] = run(gs,problem)
% rng default
% func = @(y)robotSimulationVelocity([x,y,0], NE, NT, goals, 100, 0);
% opts = optimoptions('particleswarm','InitialSwarmMatrix', [0:0.01:1].', 'display', 'iter');
% [xpso,fpso,flgpso,opso] = particleswarm(func,1,[],[],opts)

toc
x1 = [x, y, 0];
error1 = robotSimulationVelocity(x1, NE, NT, goals, 100, 0);
%%
% figure
% hold on
errs = zeros(11,11);
for y = 0:0.01:0.1
	y
	for l = 0:1:10
		errs(round(1+100*y),round(1+1*l)) = robotSimulationVelocity([x,y,l], NE, NT, goals, 100, 0);
% 		drawnow;
	end
end
surf(errs)

%%
figure
hold on
% for y = 0.0146:0.000001:0.0148
% for l = 0:.1:10
for s = 0:.1:10
% 	plot(y, robotSimulationVelocity([x,y,0,1], NE, NT, goals, 100, 0), 'x');
% 	plot(l, robotSimulationVelocity([x3,l,1], NE, NT, goals, 100, 0), 'x');
	plot(s, robotSimulationVelocity([x5,s], NE, NT, goals, 100, 0), 'x');
	drawnow;
end


%% evolve together
x2 = [-1.5,-7.5,stdev,6,-8,0.88,-4, 0];
tic
x2 = fminunc(@(x)robotSimulationVelocity([x], NE, NT, goals, 100, 0), [x1(1:8),1,3.8], optimset('display', 'iter'));
% x = fminsearch(@(x)robotSimulationVelocity(x, NE, 5000, 15, 0), x, optimset('display', 'iter'))
toc
error2 = robotSimulationVelocity([x2], NE, NT, goals, 100, 0);

%% plot 2
% v = VideoWriter('peaks.mp4','MPEG-4');
% open(v);
% for i = 1:5:5000
% 	polarplot(angles, goals(:,i), 'g');
% 	hold on
% 	polarplot(angles, Es(:,i), 'b--');
% 	polarplot(angles, Es2(:,i), 'r--');
% 	rlim([0,1]);
% 	polarplot([deg2rad(thetas(i)),deg2rad(thetas(i))], [0,1], 'm');
% 	polarplot([deg2rad(means(i)),deg2rad(means(i))], [0,1], 'b');
% 	polarplot([deg2rad(means2(i)),deg2rad(means2(i))], [0,1], 'r');
% 	hold off
% 	drawnow;
% 	frame = getframe(gcf);
% 	writeVideo(v,frame);
% end
% close(v);


%% other stuff

% 20 degree network works at up to ~200 deg/s

vWeight = -5000:20:5000;
avgVs = zeros(size(vWeight));

figure
hold on
for n = 1:length(vWeight)
	avgVs(n) = robotSimulationVelocity([x2,0,1], NE, 2000, zeros(NE,2000), vWeight(n), 0);
	plot(vWeight(n), avgVs(n), 'r.');
	drawnow;
end

% vs = 0:5:500;
% errors = zeros(size(vs));
% for n = 1:length(vs)
% 	errors(n) = robotSimulationVelocity(x, NE, 5000, 20, vs(n), 0);
% end
