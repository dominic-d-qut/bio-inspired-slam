%% Optimise the Head Direction network
NE_HD = 200; % number of units
stdev_HD = 15; % degrees
[HDparams, HDerror] = OptimiseHeadDirectionAttractor(NE_HD, stdev_HD, 0.001);


%% Optimise the Grid Cell network
NE_GC = 20; % number of units
stdev_GC = 0.08; % fraction of 1 grid
[GCparams,GCerror] = OptimiseGridCellAttractor(NE_GC, stdev_GC, 0.001);

%% Run the simulation
rng('default');
[errorXY, errorTheta, xs, ys, thetas, landmarkEnergies, lastLapErrorXY] = robotSimulation3d([HDparams,x(1:2)],[GCparams,x(3:4)],NE_HD,NE_GC,20*3138+500,0);

fprintf('HD with %d cells had error = %f\nGC with %d^2 cells had error = %f\ntotal system had error [XY: %f, Theta: %f, last lap XY: %f]\n', ...
	NE_HD, HDerror, NE_GC, GCerror, errorXY, errorTheta, lastLapErrorXY);

noiseError = sum(sqrt((xs(1,:) - xs(3,:)).^2 + (ys(1,:) - ys(3,:)).^2))

%% Run the simulation with no landmark relocalisation
rng('default');
[errorXY2, errorTheta2, xs2, ys2, thetas2, landmarkEnergies2, lastLapErrorXY] = robotSimulation3d([HDparams,0,3],[GCparams,0,0.55],NE_HD,NE_GC,20*3138+500,0);

fprintf('Dead reckoning error [XY: %f, Theta: %f, last lap XY: %f]\n',  errorXY2, errorTheta2, lastLapErrorXY);

%% X, Y, Theta
t = 0.001:0.001:0.001*size(xs,2);
figure
subplot(3,1,1)
plot(t, xs(2,:)-xs(1,:), t, xs(3,:)-xs(1,:), 'linewidth', 1.5);
ylabel('x position (grids)'); xlabel('time (s)');
title(sprintf('Position and orientation of the network vs. truth'));
subplot(3,1,2)
plot(t, ys(2,:)-ys(1,:), t, ys(3,:)-ys(1,:), 'linewidth', 1.5);
ylabel('y position (grids)'); xlabel('time (s)');
subplot(3,1,3)
plot(t, wrapTo180(thetas(2,:)-thetas(1,:)), t, wrapTo180(thetas(3,:)-thetas(1,:)), 'linewidth', 1.5);
ylabel('orientation (\circ)'); xlabel('time (s)');
legend('SLAM', 'dead reckoning', 'location', 'northeast')
% subplot(4,1,4)
% plot(t, landmarkEnergies, 'linewidth', 1.5);
% ylabel('landmark activity'); xlabel('time (s)');

%% plot environment paths with no noise
landmarks = [1 .8 .3 .5 .4 0.1; .5 1 .9 .45 0 .6];
f = figure();
set(f, 'position', [403,47,560,500]);
lapLength = 3138;
% plot(xs(1,:), ys(1,:), 'g', xs(2,:), ys(2,:), xs2(2,:), ys2(2,:), 'linewidth', 1.5);
subplot(6,4,[1,2,5,6])
% plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,1:lapLength+500), ys(2,1:lapLength+500), xs2(2,1:lapLength+500), ys2(2,1:lapLength+500), xs(3,1:lapLength+500), ys(3,1:lapLength+500), 'm', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,1:lapLength+500), ys(2,1:lapLength+500), xs2(2,1:lapLength+500), ys2(2,1:lapLength+500), 'linewidth', 1.5);
legend('truth', 'SLAM', 'dead reckoning')
title('lap 1')
axis equal
% hold on
% plot(xs(3,1:end), ys(3,1:end), 'm', 'linewidth', 1.5)
subplot(6,4,[3,4,7,8])
% plot(xs(2,4*lapLength+501:5*lapLength+500), ys(2,4*lapLength+501:5*lapLength+500), 'b--', xs2(2,4*lapLength+501:5*lapLength+500), ys2(2,4*lapLength+501:5*lapLength+500), 'r--', xs(3,4*lapLength+501:5*lapLength+500), ys(3,4*lapLength+501:5*lapLength+500), 'm--', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,4*lapLength+501:5*lapLength+500), ys(2,4*lapLength+501:5*lapLength+500), 'b', xs2(2,4*lapLength+501:5*lapLength+500), ys2(2,4*lapLength+501:5*lapLength+500), 'r', 'linewidth', 1.5);
axis equal
title('lap 5')
subplot(6,4,[9,10,13,14])
% plot(xs(2,9*lapLength+501:10*lapLength+500), ys(2,9*lapLength+501:10*lapLength+500), 'b-.', xs2(2,9*lapLength+501:10*lapLength+500), ys2(2,9*lapLength+501:10*lapLength+500), 'r-.', xs(3,9*lapLength+501:10*lapLength+500), ys(3,9*lapLength+501:10*lapLength+500), 'm-.', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,9*lapLength+501:10*lapLength+500), ys(2,9*lapLength+501:10*lapLength+500), 'b', xs2(2,9*lapLength+501:10*lapLength+500), ys2(2,9*lapLength+501:10*lapLength+500), 'r', 'linewidth', 1.5);
axis equal
title('lap 10')
subplot(6,4,[11,12,15,16])
% plot(xs(2,19*lapLength+501:end), ys(2,19*lapLength+501:end), 'b:', xs2(2,19*lapLength+501:end), ys2(2,19*lapLength+501:end), 'r:', xs(3,19*lapLength+501:end), ys(3,19*lapLength+501:end), 'm:', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,19*lapLength+501:end), ys(2,19*lapLength+501:end), 'b', xs2(2,19*lapLength+501:end), ys2(2,19*lapLength+501:end), 'r', 'linewidth', 1.5);
axis equal
title('lap 20')

% plot(landmarks(1,:), landmarks(2,:), 'ko','markersize', 10, 'markerfacecolor', 'black');
% plot(xs(1,1), ys(1,1), 'kx', 'markersize', 20)
% hold off
% legend('truth', 'localisation lap 1', 'dead reckoning lap 1', 'numerical integration lap 1', 'localisation lap 5', 'dead reckoning lap 5', ...
% 	'numerical integration lap 5', 'localisation lap 10', 'dead reckoning lap 10', 'numerical integration lap 10', 'localisation lap 20', ...
% 	'dead reckoning lap 20', 'numerical integration lap 20', 'landmarks', 'start/finish', 'location', 'northeast')

%% plot environment paths with noise
landmarks = [1 .8 .3 .5 .4 0.1; .5 1 .9 .45 0 .6];
f = figure();
set(f, 'position', [403,47,560,500]);
lapLength = 4603;
% plot(xs(1,:), ys(1,:), 'g', xs(2,:), ys(2,:), xs2(2,:), ys2(2,:), 'linewidth', 1.5);
subplot(6,4,[1,2,5,6])
% plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,1:lapLength+500), ys(2,1:lapLength+500), xs2(2,1:lapLength+500), ys2(2,1:lapLength+500), xs(3,1:lapLength+500), ys(3,1:lapLength+500), 'm', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,1:lapLength+500), ys(2,1:lapLength+500), xs2(2,1:lapLength+500), ys2(2,1:lapLength+500), xs(3,1:lapLength+500), ys(3,1:lapLength+500), 'm','linewidth', 1.5);
legend('truth', 'SLAM', 'dead reckoning', 'numerical integration')
title('lap 1')
axis equal
% hold on
% plot(xs(3,1:end), ys(3,1:end), 'm', 'linewidth', 1.5)
subplot(6,4,[3,4,7,8])
% plot(xs(2,4*lapLength+501:5*lapLength+500), ys(2,4*lapLength+501:5*lapLength+500), 'b--', xs2(2,4*lapLength+501:5*lapLength+500), ys2(2,4*lapLength+501:5*lapLength+500), 'r--', xs(3,4*lapLength+501:5*lapLength+500), ys(3,4*lapLength+501:5*lapLength+500), 'm--', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,4*lapLength+501:5*lapLength+500), ys(2,4*lapLength+501:5*lapLength+500), 'b', xs2(2,4*lapLength+501:5*lapLength+500), ys2(2,4*lapLength+501:5*lapLength+500), 'r', xs(3,4*lapLength+501:5*lapLength+500), ys(3,4*lapLength+501:5*lapLength+500), 'm','linewidth', 1.5);
axis equal
title('lap 5')
subplot(6,4,[9,10,13,14])
% plot(xs(2,9*lapLength+501:10*lapLength+500), ys(2,9*lapLength+501:10*lapLength+500), 'b-.', xs2(2,9*lapLength+501:10*lapLength+500), ys2(2,9*lapLength+501:10*lapLength+500), 'r-.', xs(3,9*lapLength+501:10*lapLength+500), ys(3,9*lapLength+501:10*lapLength+500), 'm-.', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,9*lapLength+501:10*lapLength+500), ys(2,9*lapLength+501:10*lapLength+500), 'b', xs2(2,9*lapLength+501:10*lapLength+500), ys2(2,9*lapLength+501:10*lapLength+500), 'r', xs(3,9*lapLength+501:10*lapLength+500), ys(3,9*lapLength+501:10*lapLength+500), 'm','linewidth', 1.5);
axis equal
title('lap 10')
subplot(6,4,[11,12,15,16])
% plot(xs(2,19*lapLength+501:end), ys(2,19*lapLength+501:end), 'b:', xs2(2,19*lapLength+501:end), ys2(2,19*lapLength+501:end), 'r:', xs(3,19*lapLength+501:end), ys(3,19*lapLength+501:end), 'm:', 'linewidth', 1.5);
plot(xs(1,1:lapLength+500), ys(1,1:lapLength+500), 'g', xs(2,19*lapLength+501:end), ys(2,19*lapLength+501:end), 'b', xs2(2,19*lapLength+501:end), ys2(2,19*lapLength+501:end), 'r', xs(3,19*lapLength+501:end), ys(3,19*lapLength+501:end), 'm','linewidth', 1.5);
axis equal
title('lap 20')

% plot(landmarks(1,:), landmarks(2,:), 'ko','markersize', 10, 'markerfacecolor', 'black');
% plot(xs(1,1), ys(1,1), 'kx', 'markersize', 20)
% hold off

%% error vs time
t = 0.001:0.001:0.001*size(xs,2);
% figure
% subplot(6,4,[17:24])
errors1 = sqrt((xs(2,:) - xs(1,:)).^2 + (ys(2,:) - ys(1,:)).^2);
errors2 = sqrt((xs2(2,:) - xs2(1,:)).^2 + (ys2(2,:) - ys2(1,:)).^2);
errors3 = sqrt((xs(3,:) - xs(1,:)).^2 + (ys(3,:) - ys(1,:)).^2);
plot(t, errors1, 'b', t, errors2, 'r', t, errors3, 'm', 'linewidth', 1.5);
legend('localisation', 'dead reckoning', 'numerical integration');
% plot(t, errors1, 'b', t, errors2, 'r', 'linewidth', 1.5);
% legend('localisation', 'dead reckoning');
title('Position error over time')
ylabel('Position error (grids)')
xlabel('time (s)')
set(f, 'position', [403,47,560,620]);

%% real time plot
t = 0.001:0.001:0.001*size(xs,2);

groundTruth = [xs(1,:);ys(1,:)];

errors1 = sqrt((xs(2,:) - xs(1,:)).^2 + (ys(2,:) - ys(1,:)).^2);
errors2 = sqrt((xs2(2,:) - xs2(1,:)).^2 + (ys2(2,:) - ys2(1,:)).^2);
errors3 = sqrt((xs(3,:) - xs(1,:)).^2 + (ys(3,:) - ys(1,:)).^2);

f = figure
set(f, 'position', [403,160,1920,520]);

subplot(2,6,[1,2,7,8]);
plot(groundTruth(1,:),groundTruth(2,:),'g');
axis equal;
title('SLAM');
hold on;

subplot(2,6,[3,4,9,10]);
plot(groundTruth(1,:),groundTruth(2,:),'g');
axis equal;
title('Odometry integration');
hold on;

subplot(2,6,[5,6,11,12]);
plot(groundTruth(1,:),groundTruth(2,:),'g');
axis equal;
title('Dead reckoning');
hold on;

% figure
% subplot(4,6,[17:24])
% title('Position error over time')
% ylabel('Position error (grids)')
% xlabel('time (s)')
% xlim([0,t(end)]);
% hold on;

set(f, 'position', [403,160,1920,400]);
set(f, 'color', 'w');


vid = VideoWriter('figure8Error.mp4','MPEG-4');
vid.FrameRate = 30;
open(vid);

N = 150;
for i = 1:ceil(size(errors1,2)/N)
	range = (i-1)*N + 1:i*N;
	if i == ceil(size(errors1,2)/N)
		range = (i-1)*N + 1:size(errors1,2);
	end
	
	subplot(2,6,[1,2,7,8]);
	plot(xs(2,range), ys(2,range), 'b', 'linewidth', 1.5);
	
% 	subplot(4,6,[17:24])
% 	plot(t(range), errors1(range), 'b', 'linewidth', 1.5);
	drawnow;
	f = gcf;
	frame = getframe(f);
	writeVideo(vid,frame);
end
for i = 1:ceil(size(errors1,2)/N)
	range = (i-1)*N + 1:i*N;
	if i == ceil(size(errors1,2)/N)
		range = (i-1)*N + 1:size(errors1,2);
	end
	
	subplot(2,6,[3,4,9,10]);
	plot(xs2(2,range), ys2(2,range), 'r', 'linewidth', 1.5);

% 	subplot(4,6,[17:24])
% 	plot(t(range), errors2(range), 'r', 'linewidth', 1.5);
	drawnow;
	f = gcf;
	frame = getframe(f);
	writeVideo(vid,frame);
end
for i = 1:ceil(size(errors1,2)/N)
	range = (i-1)*N + 1:i*N;
	if i == ceil(size(errors1,2)/N)
		range = (i-1)*N + 1:size(errors1,2);
	end
	
	subplot(2,6,[5,6,11,12]);
	plot(xs(3,range), ys(3,range), 'm', 'linewidth', 1.5);
	
% 	subplot(4,6,[17:24])
% 	plot(t(range), errors3(range), 'm', 'linewidth', 1.5);
	drawnow;
	f = gcf;
	frame = getframe(f);
	writeVideo(vid,frame);
end
close(vid);

%% optimise SLAM landmark parameters
tic;
[x,fval,exitflag,output,population,scores] = ga(@(x)robotSimulation3d([HDparams,x(1:2)],[GCparams,x(3:4)],NE_HD,NE_GC,5*3138+500,0), 4, [],[],[],[],...
	[0, 1e-6,0,1e-6],[Inf, 360, Inf,1],[], optimoptions('ga','display','iter','useparallel',1,'populationsize', 10, 'maxGenerations', 20));
toc;