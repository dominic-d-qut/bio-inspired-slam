goals = zeros(NE,2000);
theta = 0;
anglesDeg = rad2deg(angles);
for t = 1:2000
	% update the robot theta
	if t > 0.2/dt && t <= (2000*dt - 0.2)/dt
		vRot = 100;
	else
		vRot = 0;
	end
	theta = wrapTo360(theta + dt*vRot);
	goals(:,t) = circularnormpdf(anglesDeg, theta, 30, 360).';
end

robotSimulationVelocity([x,y,0,1], NE, 2000, goals, 100, 0)