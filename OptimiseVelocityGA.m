NE = 100;
stdev = 20;
dt = 0.001;

% initial state
x0 = [-1.5,-7.5,stdev,6,-8,0.88,-4];
angles = linspace(0,2*pi,NE+1);
angles = angles(1:end-1);


%% setup goals
NT = 2000;
speed = 100;

goals = zeros(NE, NT);
theta = 0;
anglesDeg = rad2deg(angles);
for t = 1:NT
	% update the robot theta
	if t > 0.2/dt && t <= (NT*dt - 0.2)/dt
		vRot = speed;
	else
		vRot = 0;
	end
	theta = wrapTo360(theta + dt*vRot);
	d = circularnormpdf(anglesDeg, theta, stdev, 360).';
	goals(:,t) = d / max(d);
end

fun = @(x) robotSimulationVelocity(x, NE, NT, goals, speed, 0);
lowerbounds = [-Inf, -Inf, 0, 0, -Inf, 0, -Inf, 0, 0, 1e-6];
upperbounds = [0, 0, Inf, Inf, 0, Inf, 0, Inf, Inf, Inf];

tic;
x = ga(fun, 10, [], [], [], [], lowerbounds, upperbounds, [], optimset('display', 'iter'))
toc;