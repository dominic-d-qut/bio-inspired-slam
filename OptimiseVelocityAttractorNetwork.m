% 
tLength = 600;
tStartVelocity = 0.2;
tEndVelocity = 0.4;
IN = zeros(NE,tLength);
IN(spikeLocation,1:100) = 1;

v = -2:0.01:2;
avgVs = zeros(length(stdevRange), length(v));
vCurves = zeros(length(stdevRange), length(v), tLength);
tic
for k = 1:length(stdevRange)
	k
	for n = 1:length(v)
		[avgV,vCurve] = RunVelocityAttractor(xs(k,:), IN, v(n), NE, 0.001, tStartVelocity, tEndVelocity, 0);
		avgVs(k,n) = avgV;
		vCurves(k,n,:) = vCurve;
	end
%  	plot(v, avgVs(k,:), 'rx');
%  	title(sprintf('velocity tuning curve - target stdev of stable packet = %.1f degrees',stdevRange(k)));
%  	ylabel('degrees per second');
%  	xlabel('cells copy distance');
% 	figure
% 	surf(reshape(vCurves(k,:,:),[length(v),tLength,1]));
% 	shading flat
end
toc

vw = -200:1:200;
avgv_vws = zeros(1,length(vw));
stdevs = zeros(1, length(vw));
for n = 1:length(vw)
	[avgV,vCurve,stdev] = RunVelocityAttractor(xs(20,:), IN, 0.5, NE, 0.001, tStartVelocity, tEndVelocity, 0, vw(n));
	avgv_vws(n) = avgV;
	stdevs(n) = stdev;
end
plot(vw, avgv_vws, 'r.')
title('velocity tuning curve - target stdev of stable packet = 10 degrees');
ylabel('network packet velocity (\circ/s)');
xlabel('velocity copy weight');
figure
plot(vw, stdevs, 'r.')
title('velocity tuning curve - target stdev of stable packet = 10 degrees');
ylabel('network packet stdev (\circ)');
xlabel('velocity copy weight');
% RunVelocityAttractor(xs(10,:), IN, 0.5, NE, 0.001, tStartVelocity, tEndVelocity, 1, 40); % all good
% RunVelocityAttractor(xs(10,:), IN, 0.5, NE, 0.001, tStartVelocity, tEndVelocity, 1, 42); % double pointed travelling wave
% RunVelocityAttractor(xs(10,:), IN, 0.5, NE, 0.001, tStartVelocity, tEndVelocity, 1, 75); % wave breaks down instead of settling

% RunVelocityAttractor(xs(20,:), IN, 0.5, NE, 0.001, tStartVelocity, tEndVelocity, 1, 30); % 
% RunVelocityAttractor(xs(20,:), IN, 0.5, NE, 0.001, tStartVelocity, tEndVelocity, 1, 50); % 

for k = 1:70%length(stdevRange)
 	plot(v, avgVs(k,:), 'r.');
 	title(sprintf('velocity tuning curve - target stdev of stable packet = %.1f degrees',stdevRange(k)));
 	ylabel('degrees per second');
 	xlabel('cells copy distance');
	ylim([-1000,1000]);
	drawnow;
	f = gcf;
	frame = getframe(f);
	im = frame2im(frame);
	[imind,cm] = rgb2ind(im,256);
	if k == 1 
		 imwrite(imind,cm,'animation.gif','gif', 'Loopcount',inf,'delaytime',0.5); 
	else 
		 imwrite(imind,cm,'animation.gif','gif','WriteMode','append','delaytime',0.5); 
	end
% 	figure
% 	surf(reshape(vCurves(k,:,:),[length(v),tLength,1]));
% 	shading flat
end
% 
figure
scatter(reshape(repmat(v,61,1),[],1), reshape(avgVs(10:70,:),[],1), 10,'filled', 'r', 'markerfacealpha', 0.02);
title('overlaid velocity tuning curves for stdev = 10 to 70');
ylabel('degrees per second');
xlabel('cells copy distance');
ylim([-600,600]);
% 
% 
% [x,y] = meshgrid(v , stdevRange(5:70));
% surf(x, y, avgVs(5:70,:));
% shading flat;
% xlabel('cells copy distance');
% ylabel('acivity packet stdev (\circ)');
% zlabel('network packet velocity (\circ/s)');
% title('network packet velocity vs. cells copy distance and activity packet stdev');
% 

s = 5;
c = 20.0;
plot(reshape(vCurves(s,find(abs(v-c) < 1e-5, 1), :), [], 1, 1))
title(sprintf('velocity vs. time for stdev = %d and cell copy distance = %.1f', s, c))
xlabel('time, t (s)')
ylabel('network packet velocity (\circ/s)');