# Bio-inspired SLAM

## To run an already calibrated network (quick start)
- Load matfiles/figure8.mat
- Open runSimulation3d.m
- Run section 3 of the code ("Run the simulation")
- The simulation should run and be displayed

## To calibrate a network for a new environment
- If you wish to use the same network size:
	- Head direction = 100 cells, stdev = 20 degrees
	- Grid cell = 10x10 cells, stdev = 0.1
- Add a new environment specified in robotSimulation3d.m
- Load the HD and GC stability and velocity calibration values from figure8.mat or office.mat
- Change the outputs of robotSimulation3d.m:
	- From [errorXY, errorTheta, xs, ys, thetas, landmarkEnergies, lastLapErrorXY]
	- To [lastLapErrorXY]
	- this calibrates the landmark localisation to minimise error on the 5th lap, which gives better results than using the total error over the first 5 laps
- Run the last section in runSimulation3d.m ("optimise SLAM landmark parameters") [this will take ~half an hour or so depending on the size of your environment / number of laps]
- Save the parameters in a mat file for later!

## To calibrate a new network for stability and velocity integration
- Specify network size and desired activity packet standard deviation 
- Run OptimiseHeadDirectionAttractor.m and OptimiseGridCellAttractor.m
- Save the parameters in a mat file for later! Then train for landmark localisation.

## To inspect the stable 1D HD network:
- After training / loading the network params ``HDparams``
```matlab
% setup
angles = linspace(0,2*pi,NE_HD+1);
angles = angles(1:end-1);
spikeLocation = round(NE_HD/2);
spikeAngle = rad2deg(angles(spikeLocation));
goal = circularnormpdf(rad2deg(angles), spikeAngle, stdev_HD, 360);
goal = goal ./ max(goal);
% run the stable network
RunStableAttractor(HDparams, goal, NE_HD, 0.001, 1000, round(NE_HD/2), 0.5, 0.1, 1)
```
- You can achieve the same with the 1D or 2D network by putting a breakpoint inside the Optimise... functions